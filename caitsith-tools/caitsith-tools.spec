Summary: Userspace tools for CaitSith 0.2

Name: caitsith-tools
Version: 0.2
Release: 5
License: GPL
Group: System Environment/Kernel
ExclusiveOS: Linux
Autoreqprov: no
Buildroot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
##
## This spec file is intended to be distribution independent.
## I don't enable "BuildRequires:" line because rpmbuild will fail on
## environments where packages are managed by (e.g.) apt.
##
# BuildRequires: ncurses-devel
Requires: ncurses
Conflicts: caitsith-tools < 0.2-5

Source0: https://sourceforge.net/projects/tomoyo/files/caitsith-tools/0.2/caitsith-tools-0.2-20210910.tar.gz

%description
This package contains userspace tools for administrating CaitSith 0.2.
Please see https://caitsith.osdn.jp/ for documentation.

%prep

%setup -q -n caitsith-tools

%build

make USRLIBDIR=%_libdir CFLAGS="-Wall $RPM_OPT_FLAGS"

%install

rm -rf $RPM_BUILD_ROOT
make INSTALLDIR=$RPM_BUILD_ROOT USRLIBDIR=%_libdir install

%clean

rm -rf $RPM_BUILD_ROOT

%post
ldconfig || true

%files
%defattr(-,root,root)
/sbin/*
%_libdir/caitsith/*
%_libdir/libcaitsith*
/usr/sbin/*

%changelog
* Fri Sep 10 2021 0.2-5
- Add -DNCURSES_WIDECHAR=0 to programs using ncurses library.
  ( https://lists.gnu.org/archive/html/bug-ncurses/2021-07/msg00021.html )

* Sun Jun 06 2021 0.2-4
- Reflect policy syntax update in caitsith-patch 0.2.10.

* Mon May 15 2017 0.2-3
- Correct errors in kernel_test/ programs.

* Sun Oct 16 2016 0.2-2
- In order to cancel the effect of MS_REC|MS_SHARED done by systemd,
  mark MS_REC|MS_PRIVATE before mounting securityfs.

* Wed Oct 05 2016 0.2-1
- Use /sys/kernel/security/caitsith/ by default and fall back to
  /proc/caitsith/ .

* Thu Jul 23 2015 0.1-5
- caitsith-queryd: Copy a line to edit buffer correctly.

* Sun Jan 05 2014 0.1-4
- Let caitsith-queryd use poll() rather than select().

* Thu Feb 14 2013 0.1-3
- Change Makefile's build flags, as suggested by Simon Ruderich and Hideki
  Yamane. (Debian bug 674723)
- Change / to /* in rpm's %files section because Fedora 18 complains conflicts.

* Sat May 05 2012 0.1-2
- caitsith-init: Count number of ACL entries correctly.

* Sun Apr 01 2012 0.1-1
- First-release.
