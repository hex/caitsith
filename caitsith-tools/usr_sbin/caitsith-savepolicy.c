/*
 * caitsith-savepolicy.c
 *
 * CaitSith's utilities.
 *
 * Copyright (C) 2005-2012  NTT DATA CORPORATION
 *
 * Version: 0.2   2016/10/05
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License v2 as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 */
#include "caitsithtools.h"

/**
 * ccs_move_proc_to_file - Save /sys/kernel/security/caitsith/ to /etc/caitsith/ .
 *
 * @src:  Filename to save from.
 * @dest: Filename to save to.
 *
 * Returns true on success, false otherwise.
 */
static _Bool ccs_move_proc_to_file(const char *src, const char *dest)
{
	FILE *proc_fp = ccs_open_read(src);
	FILE *file_fp;
	_Bool result = true;
	if (!proc_fp) {
		fprintf(stderr, "Can't open %s for reading.\n", src);
		return false;
	}
	file_fp = dest ? fopen(dest, "w") : stdout;
	if (!file_fp) {
		fprintf(stderr, "Can't open %s for writing.\n", dest);
		fclose(proc_fp);
		return false;
	}
	while (true) {
		const int c = fgetc(proc_fp);
		if (ccs_network_mode && !c)
			break;
		if (c == EOF)
			break;
		if (fputc(c, file_fp) == EOF)
			result = false;
	}
	fclose(proc_fp);
	if (file_fp != stdout)
		if (fclose(file_fp) == EOF)
			result = false;
	return result;
}

static const char *ccs_policy_dir = NULL;

static _Bool ccs_cat_file(const char *path)
{
	FILE *fp = ccs_open_read(path);
	_Bool result = true;
	if (!fp) {
		fprintf(stderr, "Can't open %s\n", path);
		return false;
	}
	while (true) {
		int c = fgetc(fp);
		if (ccs_network_mode && !c)
			break;
		if (c == EOF)
			break;
		if (putchar(c) == EOF)
			result = false;
	}
	fclose(fp);
	return result;
}

static _Bool ccs_save_policy(void)
{
	time_t now = time(NULL);
	char stamp[32] = { };
	while (1) {
		struct tm *tm = localtime(&now);
		snprintf(stamp, sizeof(stamp) - 1,
			 "%02d-%02d-%02d.%02d:%02d:%02d",
			 tm->tm_year % 100, tm->tm_mon + 1, tm->tm_mday,
			 tm->tm_hour, tm->tm_min, tm->tm_sec);
		if (access(stamp, F_OK))
			break;
		else if (errno == EEXIST)
			now++;
		else {
			fprintf(stderr, "Can't create %s/policy/%s .\n",
				ccs_policy_dir, stamp);
			return false;
		}
	}
	if (!ccs_move_proc_to_file(CCS_PROC_POLICY_POLICY, stamp) ||
	    (rename("current", "previous") && errno != ENOENT) ||
	    symlink(stamp, "current")) {
		fprintf(stderr, "Failed to save policy.\n");
		return false;
	}
	return true;
}

int main(int argc, char *argv[])
{
	_Bool use_stdout = false;
	int i;
	for (i = 1; i < argc; i++) {
		char *ptr = argv[i];
		char *cp = strchr(ptr, ':');
		if (*ptr == '/') {
			if (ccs_policy_dir || use_stdout)
				goto usage;
			ccs_policy_dir = ptr;
		} else if (cp) {
			*cp++ = '\0';
			ccs_network_ip = inet_addr(ptr);
			ccs_network_port = htons(atoi(cp));
			if (ccs_network_mode) {
				fprintf(stderr, "You cannot specify multiple "
					"%s at the same time.\n\n",
					"remote agents");
				goto usage;
			}
			ccs_network_mode = true;
		} else if (*ptr++ == '-' && !*ptr) {
			if (ccs_policy_dir || use_stdout)
				goto usage;
			use_stdout = true;
		} else
			goto usage;
	}
	if (ccs_network_mode)
		ccs_check_remote_host(true);
	else
		ccs_check_policy_dir(true);
	if (use_stdout)
		return !ccs_cat_file(CCS_PROC_POLICY_POLICY);
	if (!ccs_policy_dir)
		ccs_policy_dir = "/etc/caitsith";
	if (chdir(ccs_policy_dir) || chdir("policy/")) {
		fprintf(stderr, "Directory %s/policy/ doesn't exist.\n",
			ccs_policy_dir);
		return 1;
	}
	return !ccs_save_policy();
usage:
	printf("Usage: %s [policy_dir|-] [remote_ip:remote_port]\n\n"
	       "policy_dir : Use policy_dir rather than /etc/caitsith "
	       "directory.\n"
	       "- : Print policy to stdout rather than save as a file.\n"
	       "remote_ip:remote_port : Read from caitsith-agent listening at "
	       "remote_ip:remote_port .\n", argv[0]);
	return 1;
}
