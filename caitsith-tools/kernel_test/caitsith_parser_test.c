/*
 * caitsith-parser-test.c
 *
 * Copyright (C) 2012-2013  Tetsuo Handa
 *
 * Version: 0.2   2021/06/06
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License v2 as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <sys/mount.h>
#ifndef MS_REC
#define MS_REC      16384
#endif
#ifndef MS_PRIVATE
#define MS_PRIVATE  (1 << 18)
#endif

enum ccs_mac_index {
	CCS_MAC_EXECUTE,
	CCS_MAC_READ,
	CCS_MAC_WRITE,
	CCS_MAC_APPEND,
	CCS_MAC_CREATE,
	CCS_MAC_UNLINK,
	CCS_MAC_GETATTR,
	CCS_MAC_MKDIR,
	CCS_MAC_RMDIR,
	CCS_MAC_MKFIFO,
	CCS_MAC_MKSOCK,
	CCS_MAC_TRUNCATE,
	CCS_MAC_SYMLINK,
	CCS_MAC_MKBLOCK,
	CCS_MAC_MKCHAR,
	CCS_MAC_LINK,
	CCS_MAC_RENAME,
	CCS_MAC_CHMOD,
	CCS_MAC_CHOWN,
	CCS_MAC_CHGRP,
	CCS_MAC_IOCTL,
	CCS_MAC_CHROOT,
	CCS_MAC_MOUNT,
	CCS_MAC_UMOUNT,
	CCS_MAC_PIVOT_ROOT,
	CCS_MAC_INET_STREAM_BIND,
	CCS_MAC_INET_STREAM_LISTEN,
	CCS_MAC_INET_STREAM_CONNECT,
	CCS_MAC_INET_STREAM_ACCEPT,
	CCS_MAC_INET_DGRAM_BIND,
	CCS_MAC_INET_DGRAM_SEND,
	CCS_MAC_INET_DGRAM_RECV,
	CCS_MAC_INET_RAW_BIND,
	CCS_MAC_INET_RAW_SEND,
	CCS_MAC_INET_RAW_RECV,
	CCS_MAC_UNIX_STREAM_BIND,
	CCS_MAC_UNIX_STREAM_LISTEN,
	CCS_MAC_UNIX_STREAM_CONNECT,
	CCS_MAC_UNIX_STREAM_ACCEPT,
	CCS_MAC_UNIX_DGRAM_BIND,
	CCS_MAC_UNIX_DGRAM_SEND,
	CCS_MAC_UNIX_DGRAM_RECV,
	CCS_MAC_UNIX_SEQPACKET_BIND,
	CCS_MAC_UNIX_SEQPACKET_LISTEN,
	CCS_MAC_UNIX_SEQPACKET_CONNECT,
	CCS_MAC_UNIX_SEQPACKET_ACCEPT,
	CCS_MAC_ENVIRON,
	CCS_MAC_PTRACE,
	CCS_MAC_SIGNAL,
	CCS_MAC_MODIFY_POLICY,
	CCS_MAC_USE_NETLINK_SOCKET,
	CCS_MAC_USE_PACKET_SOCKET,
	CCS_MAC_USE_REBOOT,
	CCS_MAC_USE_VHANGUP,
	CCS_MAC_SET_TIME,
	CCS_MAC_SET_PRIORITY,
	CCS_MAC_SET_HOSTNAME,
	CCS_MAC_USE_KERNEL_MODULE,
	CCS_MAC_USE_NEW_KERNEL,
	CCS_MAC_AUTO_DOMAIN_TRANSITION,
	CCS_MAC_MANUAL_DOMAIN_TRANSITION,
	CCS_MAX_MAC_INDEX
};

static const char * const ccs_mac_keywords[CCS_MAX_MAC_INDEX] = {
	[CCS_MAC_EXECUTE]                  = "execute",
	[CCS_MAC_READ]                     = "read",
	[CCS_MAC_WRITE]                    = "write",
	[CCS_MAC_APPEND]                   = "append",
	[CCS_MAC_CREATE]                   = "create",
	[CCS_MAC_UNLINK]                   = "unlink",
	[CCS_MAC_GETATTR]                  = "getattr",
	[CCS_MAC_MKDIR]                    = "mkdir",
	[CCS_MAC_RMDIR]                    = "rmdir",
	[CCS_MAC_MKFIFO]                   = "mkfifo",
	[CCS_MAC_MKSOCK]                   = "mksock",
	[CCS_MAC_TRUNCATE]                 = "truncate",
	[CCS_MAC_SYMLINK]                  = "symlink",
	[CCS_MAC_MKBLOCK]                  = "mkblock",
	[CCS_MAC_MKCHAR]                   = "mkchar",
	[CCS_MAC_LINK]                     = "link",
	[CCS_MAC_RENAME]                   = "rename",
	[CCS_MAC_CHMOD]                    = "chmod",
	[CCS_MAC_CHOWN]                    = "chown",
	[CCS_MAC_CHGRP]                    = "chgrp",
	[CCS_MAC_IOCTL]                    = "ioctl",
	[CCS_MAC_CHROOT]                   = "chroot",
	[CCS_MAC_MOUNT]                    = "mount",
	[CCS_MAC_UMOUNT]                   = "unmount",
	[CCS_MAC_PIVOT_ROOT]               = "pivot_root",
	[CCS_MAC_INET_STREAM_BIND]         = "inet_stream_bind",
	[CCS_MAC_INET_STREAM_LISTEN]       = "inet_stream_listen",
	[CCS_MAC_INET_STREAM_CONNECT]      = "inet_stream_connect",
	[CCS_MAC_INET_STREAM_ACCEPT]       = "inet_stream_accept",
	[CCS_MAC_INET_DGRAM_BIND]          = "inet_dgram_bind",
	[CCS_MAC_INET_DGRAM_SEND]          = "inet_dgram_send",
	[CCS_MAC_INET_DGRAM_RECV]          = "inet_dgram_recv",
	[CCS_MAC_INET_RAW_BIND]            = "inet_raw_bind",
	[CCS_MAC_INET_RAW_SEND]            = "inet_raw_send",
	[CCS_MAC_INET_RAW_RECV]            = "inet_raw_recv",
	[CCS_MAC_UNIX_STREAM_BIND]         = "unix_stream_bind",
	[CCS_MAC_UNIX_STREAM_LISTEN]       = "unix_stream_listen",
	[CCS_MAC_UNIX_STREAM_CONNECT]      = "unix_stream_connect",
	[CCS_MAC_UNIX_STREAM_ACCEPT]       = "unix_stream_accept",
	[CCS_MAC_UNIX_DGRAM_BIND]          = "unix_dgram_bind",
	[CCS_MAC_UNIX_DGRAM_SEND]          = "unix_dgram_send",
	[CCS_MAC_UNIX_DGRAM_RECV]          = "unix_dgram_recv",
	[CCS_MAC_UNIX_SEQPACKET_BIND]      = "unix_seqpacket_bind",
	[CCS_MAC_UNIX_SEQPACKET_LISTEN]    = "unix_seqpacket_listen",
	[CCS_MAC_UNIX_SEQPACKET_CONNECT]   = "unix_seqpacket_connect",
	[CCS_MAC_UNIX_SEQPACKET_ACCEPT]    = "unix_seqpacket_accept",
	[CCS_MAC_ENVIRON]                  = "environ",
	[CCS_MAC_PTRACE]                   = "ptrace",
	[CCS_MAC_SIGNAL]                   = "signal",
	[CCS_MAC_MODIFY_POLICY]            = "modify_policy",
	[CCS_MAC_USE_NETLINK_SOCKET]       = "use_netlink_socket",
	[CCS_MAC_USE_PACKET_SOCKET]        = "use_packet_socket",
	[CCS_MAC_USE_REBOOT]               = "use_reboot",
	[CCS_MAC_USE_VHANGUP]              = "use_vhangup",
	[CCS_MAC_SET_TIME]                 = "set_time",
	[CCS_MAC_SET_PRIORITY]             = "set_priority",
	[CCS_MAC_SET_HOSTNAME]             = "set_hostname",
	[CCS_MAC_USE_KERNEL_MODULE]        = "use_kernel_module",
	[CCS_MAC_USE_NEW_KERNEL]           = "use_new_kernel",
	[CCS_MAC_AUTO_DOMAIN_TRANSITION]   = "auto_domain_transition",
	[CCS_MAC_MANUAL_DOMAIN_TRANSITION] = "manual_domain_transition",
};

#define F(bit) (1ULL << bit)

#define CCS_ALL_OK			     \
	(F(CCS_MAC_EXECUTE) |		     \
	 F(CCS_MAC_READ) |                   \
	 F(CCS_MAC_WRITE) |		     \
	 F(CCS_MAC_APPEND) |		     \
	 F(CCS_MAC_CREATE) |                 \
	 F(CCS_MAC_UNLINK) |		     \
	 F(CCS_MAC_GETATTR) |		     \
	 F(CCS_MAC_MKDIR) |		     \
	 F(CCS_MAC_RMDIR) |		     \
	 F(CCS_MAC_MKFIFO) |		     \
	 F(CCS_MAC_MKSOCK) |		     \
	 F(CCS_MAC_TRUNCATE) |		     \
	 F(CCS_MAC_SYMLINK) |		     \
	 F(CCS_MAC_MKBLOCK) |		     \
	 F(CCS_MAC_MKCHAR) |		     \
	 F(CCS_MAC_LINK) |		     \
	 F(CCS_MAC_RENAME) |		     \
	 F(CCS_MAC_CHMOD) |		     \
	 F(CCS_MAC_CHOWN) |		     \
	 F(CCS_MAC_CHGRP) |		     \
	 F(CCS_MAC_IOCTL) |		     \
	 F(CCS_MAC_CHROOT) |		     \
	 F(CCS_MAC_MOUNT) |		     \
	 F(CCS_MAC_UMOUNT) |                 \
	 F(CCS_MAC_PIVOT_ROOT) |	     \
	 F(CCS_MAC_INET_STREAM_BIND) |	     \
	 F(CCS_MAC_INET_STREAM_LISTEN) |     \
	 F(CCS_MAC_INET_STREAM_CONNECT) |    \
	 F(CCS_MAC_INET_STREAM_ACCEPT) |     \
	 F(CCS_MAC_INET_DGRAM_BIND) |        \
	 F(CCS_MAC_INET_DGRAM_SEND) |	     \
	 F(CCS_MAC_INET_DGRAM_RECV) |	     \
	 F(CCS_MAC_INET_RAW_BIND) |	     \
	 F(CCS_MAC_INET_RAW_SEND) |	     \
	 F(CCS_MAC_INET_RAW_RECV) |	     \
	 F(CCS_MAC_UNIX_STREAM_BIND) |	     \
	 F(CCS_MAC_UNIX_STREAM_LISTEN) |     \
	 F(CCS_MAC_UNIX_STREAM_CONNECT) |    \
	 F(CCS_MAC_UNIX_STREAM_ACCEPT) |     \
	 F(CCS_MAC_UNIX_DGRAM_BIND) |	     \
	 F(CCS_MAC_UNIX_DGRAM_SEND) |	     \
	 F(CCS_MAC_UNIX_DGRAM_RECV) |	     \
	 F(CCS_MAC_UNIX_SEQPACKET_BIND) |    \
	 F(CCS_MAC_UNIX_SEQPACKET_LISTEN) |  \
	 F(CCS_MAC_UNIX_SEQPACKET_CONNECT) | \
	 F(CCS_MAC_UNIX_SEQPACKET_ACCEPT) |  \
	 F(CCS_MAC_ENVIRON) |                \
	 F(CCS_MAC_PTRACE) |                 \
	 F(CCS_MAC_SIGNAL) |                 \
	 F(CCS_MAC_MODIFY_POLICY) |          \
	 F(CCS_MAC_USE_NETLINK_SOCKET) |     \
	 F(CCS_MAC_USE_PACKET_SOCKET) |      \
	 F(CCS_MAC_USE_REBOOT) |             \
	 F(CCS_MAC_USE_VHANGUP) |            \
	 F(CCS_MAC_SET_TIME) |               \
	 F(CCS_MAC_SET_PRIORITY) |           \
	 F(CCS_MAC_SET_HOSTNAME) |           \
	 F(CCS_MAC_USE_KERNEL_MODULE) |      \
	 F(CCS_MAC_USE_NEW_KERNEL) |         \
	 F(CCS_MAC_AUTO_DOMAIN_TRANSITION) | \
	 F(CCS_MAC_MANUAL_DOMAIN_TRANSITION))

#define CCS_PATH_SELF_OK		    \
	(F(CCS_MAC_EXECUTE) |		    \
	 F(CCS_MAC_ENVIRON) |		    \
	 F(CCS_MAC_READ) |                  \
	 F(CCS_MAC_WRITE) |	            \
	 F(CCS_MAC_APPEND) |	            \
	 F(CCS_MAC_UNLINK) |	            \
	 F(CCS_MAC_GETATTR) |	            \
	 F(CCS_MAC_RMDIR) |		    \
	 F(CCS_MAC_TRUNCATE) |		    \
	 F(CCS_MAC_CHMOD) |		    \
	 F(CCS_MAC_CHOWN) |		    \
	 F(CCS_MAC_CHGRP) |		    \
	 F(CCS_MAC_IOCTL) |		    \
	 F(CCS_MAC_CHROOT) |		    \
	 F(CCS_MAC_UMOUNT))

#define CCS_PATH_PARENT_OK	  \
	(F(CCS_MAC_CREATE) |	  \
	 F(CCS_MAC_MKDIR) |	  \
	 F(CCS_MAC_MKFIFO) |	  \
	 F(CCS_MAC_MKSOCK) |	  \
	 F(CCS_MAC_SYMLINK) |	  \
	 F(CCS_MAC_MKBLOCK) |	  \
	 F(CCS_MAC_MKCHAR))

#define CCS_PATH_OK (CCS_PATH_SELF_OK | CCS_PATH_PARENT_OK)

#define CCS_RENAME_OR_LINK_OK (F(CCS_MAC_LINK) | F(CCS_MAC_RENAME))

#define CCS_EXECUTE_OR_ENVIRON_OK (F(CCS_MAC_EXECUTE) | F(CCS_MAC_ENVIRON))

#define CCS_MKDEV_OK (F(CCS_MAC_MKBLOCK) | F(CCS_MAC_MKCHAR))

#define CCS_PATH_PERM_OK      \
	(F(CCS_MAC_MKDIR) |   \
	 F(CCS_MAC_MKBLOCK) | \
	 F(CCS_MAC_MKCHAR) |  \
	 F(CCS_MAC_MKFIFO) |  \
	 F(CCS_MAC_MKSOCK) |  \
	 F(CCS_MAC_CREATE) |  \
	 F(CCS_MAC_CHMOD))

#define CCS_IP_SOCKET_OK		    \
	(F(CCS_MAC_INET_STREAM_BIND) |	    \
	 F(CCS_MAC_INET_STREAM_LISTEN) |    \
	 F(CCS_MAC_INET_STREAM_CONNECT) |   \
	 F(CCS_MAC_INET_STREAM_ACCEPT) |    \
	 F(CCS_MAC_INET_DGRAM_BIND) |       \
	 F(CCS_MAC_INET_DGRAM_SEND) |	    \
	 F(CCS_MAC_INET_DGRAM_RECV))

#define CCS_RAW_SOCKET_OK		    \
	(F(CCS_MAC_INET_RAW_BIND) |	    \
	 F(CCS_MAC_INET_RAW_SEND) |	    \
	 F(CCS_MAC_INET_RAW_RECV))

#define CCS_INET_SOCKET_OK (CCS_IP_SOCKET_OK | CCS_RAW_SOCKET_OK)

#define CCS_UNIX_SOCKET_OK		     \
	(F(CCS_MAC_UNIX_STREAM_BIND) |	     \
	 F(CCS_MAC_UNIX_STREAM_LISTEN) |     \
	 F(CCS_MAC_UNIX_STREAM_CONNECT) |    \
	 F(CCS_MAC_UNIX_STREAM_ACCEPT) |     \
	 F(CCS_MAC_UNIX_DGRAM_BIND) |	     \
	 F(CCS_MAC_UNIX_DGRAM_SEND) |	     \
	 F(CCS_MAC_UNIX_DGRAM_RECV) |	     \
	 F(CCS_MAC_UNIX_SEQPACKET_BIND) |    \
	 F(CCS_MAC_UNIX_SEQPACKET_LISTEN) |  \
	 F(CCS_MAC_UNIX_SEQPACKET_CONNECT) | \
	 F(CCS_MAC_UNIX_SEQPACKET_ACCEPT))

enum ccs_var_type {
	CCS_TYPE_INVALID,
	CCS_TYPE_NUMBER,
	CCS_TYPE_STRING,
	CCS_TYPE_IPADDR,
	CCS_TYPE_FILEPERM,
	CCS_TYPE_FILETYPE,
	CCS_TYPE_TASKTYPE,
	CCS_TYPE_ASSIGN,
};

static const struct {
	const char * const keyword;
	const enum ccs_var_type left_type;
	const enum ccs_var_type right_type;
	const unsigned long long available;
} ccs_conditions[] = {
	{ "addr",                    CCS_TYPE_STRING,   CCS_TYPE_STRING,
	  CCS_UNIX_SOCKET_OK },
	{ "argc",                    CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_EXECUTE_OR_ENVIRON_OK },
	{ "block",                   CCS_TYPE_INVALID,  CCS_TYPE_FILETYPE,
	  CCS_ALL_OK },
	{ "char",                    CCS_TYPE_INVALID,  CCS_TYPE_FILETYPE,
	  CCS_ALL_OK },
	{ "cmd",                     CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_IOCTL) | F(CCS_MAC_PTRACE) },
	{ "data",                    CCS_TYPE_STRING,   CCS_TYPE_STRING,
	  F(CCS_MAC_MOUNT) },
	{ "dev_major",               CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_MKDEV_OK },
	{ "dev_minor",               CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_MKDEV_OK },
	{ "directory",               CCS_TYPE_INVALID,  CCS_TYPE_FILETYPE,
	  CCS_ALL_OK },
	{ "domain",                  CCS_TYPE_STRING,   CCS_TYPE_STRING,
	  F(CCS_MAC_PTRACE) | F(CCS_MAC_MANUAL_DOMAIN_TRANSITION) },
	{ "envc",                    CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_EXECUTE_OR_ENVIRON_OK },
	{ "exec",                    CCS_TYPE_STRING,   CCS_TYPE_STRING,
	  CCS_EXECUTE_OR_ENVIRON_OK },
	{ "exec.fsmagic",            CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_EXECUTE_OR_ENVIRON_OK },
	{ "exec.gid",                CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_EXECUTE_OR_ENVIRON_OK },
	{ "exec.ino",                CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_EXECUTE_OR_ENVIRON_OK },
	{ "exec.major",              CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_EXECUTE_OR_ENVIRON_OK },
	{ "exec.minor",              CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_EXECUTE_OR_ENVIRON_OK },
	{ "exec.parent.fsmagic",     CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_EXECUTE_OR_ENVIRON_OK },
	{ "exec.parent.gid",         CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_EXECUTE_OR_ENVIRON_OK },
	{ "exec.parent.ino",         CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_EXECUTE_OR_ENVIRON_OK },
	{ "exec.parent.major",       CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_EXECUTE_OR_ENVIRON_OK },
	{ "exec.parent.minor",       CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_EXECUTE_OR_ENVIRON_OK },
	{ "exec.parent.perm",        CCS_TYPE_FILEPERM, CCS_TYPE_FILEPERM,
	  CCS_EXECUTE_OR_ENVIRON_OK },
	{ "exec.parent.uid",         CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_EXECUTE_OR_ENVIRON_OK },
	{ "exec.perm",               CCS_TYPE_FILEPERM, CCS_TYPE_FILEPERM,
	  CCS_EXECUTE_OR_ENVIRON_OK },
	{ "exec.type",               CCS_TYPE_FILETYPE, CCS_TYPE_FILETYPE,
	  CCS_EXECUTE_OR_ENVIRON_OK },
	{ "exec.uid",                CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_EXECUTE_OR_ENVIRON_OK },
	{ "execute_handler",         CCS_TYPE_INVALID,  CCS_TYPE_TASKTYPE,
	  CCS_ALL_OK },
	{ "fifo",                    CCS_TYPE_INVALID,  CCS_TYPE_FILETYPE,
	  CCS_ALL_OK },
	{ "file",                    CCS_TYPE_INVALID,  CCS_TYPE_FILETYPE,
	  CCS_ALL_OK },
	{ "flags",                   CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) | F(CCS_MAC_UMOUNT) },
	{ "fstype",                  CCS_TYPE_STRING,   CCS_TYPE_STRING,
	  F(CCS_MAC_MOUNT) },
	{ "gid",                     CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_CHGRP) },
	{ "group_execute",           CCS_TYPE_INVALID,  CCS_TYPE_FILEPERM,
	  CCS_ALL_OK },
	{ "group_read",              CCS_TYPE_INVALID,  CCS_TYPE_FILEPERM,
	  CCS_ALL_OK },
	{ "group_write",             CCS_TYPE_INVALID,  CCS_TYPE_FILEPERM,
	  CCS_ALL_OK },
	{ "handler",                 CCS_TYPE_ASSIGN,   CCS_TYPE_INVALID,
	  F(CCS_MAC_EXECUTE) },
	{ "ip",                      CCS_TYPE_IPADDR,   CCS_TYPE_INVALID,
	  CCS_INET_SOCKET_OK },
	{ "name",                    CCS_TYPE_STRING,   CCS_TYPE_STRING,
	  F(CCS_MAC_ENVIRON) },
	{ "new_path",                CCS_TYPE_STRING,   CCS_TYPE_STRING,
	  CCS_RENAME_OR_LINK_OK },
	{ "new_path.dev_major",      CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_RENAME) },
	{ "new_path.dev_minor",      CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_RENAME) },
	{ "new_path.fsmagic",        CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_RENAME) },
	{ "new_path.gid",            CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_RENAME) },
	{ "new_path.ino",            CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_RENAME) },
	{ "new_path.major",          CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_RENAME) },
	{ "new_path.minor",          CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_RENAME) },
	{ "new_path.parent.fsmagic", CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_RENAME_OR_LINK_OK },
	{ "new_path.parent.gid",     CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_RENAME_OR_LINK_OK },
	{ "new_path.parent.ino",     CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_RENAME_OR_LINK_OK },
	{ "new_path.parent.major",   CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_RENAME_OR_LINK_OK },
	{ "new_path.parent.minor",   CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_RENAME_OR_LINK_OK },
	{ "new_path.parent.perm",    CCS_TYPE_FILEPERM, CCS_TYPE_FILEPERM,
	  CCS_RENAME_OR_LINK_OK },
	{ "new_path.parent.uid",     CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_RENAME_OR_LINK_OK },
	{ "new_path.perm",           CCS_TYPE_FILEPERM, CCS_TYPE_FILEPERM,
	  F(CCS_MAC_RENAME) },
	{ "new_path.type",           CCS_TYPE_FILETYPE, CCS_TYPE_FILETYPE,
	  F(CCS_MAC_RENAME) },
	{ "new_path.uid",            CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_RENAME) },
	{ "new_root",                CCS_TYPE_STRING,   CCS_TYPE_STRING,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "new_root.dev_major",      CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "new_root.dev_minor",      CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "new_root.fsmagic",        CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "new_root.gid",            CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "new_root.ino",            CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "new_root.major",          CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "new_root.minor",          CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "new_root.parent.fsmagic", CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "new_root.parent.gid",     CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "new_root.parent.ino",     CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "new_root.parent.major",   CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "new_root.parent.minor",   CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "new_root.parent.perm",    CCS_TYPE_FILEPERM, CCS_TYPE_FILEPERM,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "new_root.parent.uid",     CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "new_root.perm",           CCS_TYPE_FILEPERM, CCS_TYPE_FILEPERM,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "new_root.type",           CCS_TYPE_FILETYPE, CCS_TYPE_FILETYPE,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "new_root.uid",            CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "old_path",                CCS_TYPE_STRING,   CCS_TYPE_STRING,
	  CCS_RENAME_OR_LINK_OK },
	{ "old_path.dev_major",      CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_RENAME_OR_LINK_OK },
	{ "old_path.dev_minor",      CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_RENAME_OR_LINK_OK },
	{ "old_path.fsmagic",        CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_RENAME_OR_LINK_OK },
	{ "old_path.gid",            CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_RENAME_OR_LINK_OK },
	{ "old_path.ino",            CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_RENAME_OR_LINK_OK },
	{ "old_path.major",          CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_RENAME_OR_LINK_OK },
	{ "old_path.minor",          CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_RENAME_OR_LINK_OK },
	{ "old_path.parent.fsmagic", CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_RENAME_OR_LINK_OK },
	{ "old_path.parent.gid",     CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_RENAME_OR_LINK_OK },
	{ "old_path.parent.ino",     CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_RENAME_OR_LINK_OK },
	{ "old_path.parent.major",   CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_RENAME_OR_LINK_OK },
	{ "old_path.parent.minor",   CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_RENAME_OR_LINK_OK },
	{ "old_path.parent.perm",    CCS_TYPE_FILEPERM, CCS_TYPE_FILEPERM,
	  CCS_RENAME_OR_LINK_OK },
	{ "old_path.parent.uid",     CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_RENAME_OR_LINK_OK },
	{ "old_path.perm",           CCS_TYPE_FILEPERM, CCS_TYPE_FILEPERM,
	  CCS_RENAME_OR_LINK_OK },
	{ "old_path.type",           CCS_TYPE_FILETYPE, CCS_TYPE_FILETYPE,
	  CCS_RENAME_OR_LINK_OK },
	{ "old_path.uid",            CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_RENAME_OR_LINK_OK },
	{ "others_execute",          CCS_TYPE_INVALID,  CCS_TYPE_FILEPERM,
	  CCS_ALL_OK },
	{ "others_read",             CCS_TYPE_INVALID,  CCS_TYPE_FILEPERM,
	  CCS_ALL_OK },
	{ "others_write",            CCS_TYPE_INVALID,  CCS_TYPE_FILEPERM,
	  CCS_ALL_OK },
	{ "owner_execute",           CCS_TYPE_INVALID,  CCS_TYPE_FILEPERM,
	  CCS_ALL_OK },
	{ "owner_read",              CCS_TYPE_INVALID,  CCS_TYPE_FILEPERM,
	  CCS_ALL_OK },
	{ "owner_write",             CCS_TYPE_INVALID,  CCS_TYPE_FILEPERM,
	  CCS_ALL_OK },
	{ "path",                    CCS_TYPE_STRING,   CCS_TYPE_STRING,
	  CCS_PATH_OK },
	{ "path.dev_major",          CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_PATH_SELF_OK },
	{ "path.dev_minor",          CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_PATH_SELF_OK },
	{ "path.fsmagic",            CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_PATH_SELF_OK },
	{ "path.gid",                CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_PATH_SELF_OK },
	{ "path.ino",                CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_PATH_SELF_OK },
	{ "path.major",              CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_PATH_SELF_OK },
	{ "path.minor",              CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_PATH_SELF_OK },
	{ "path.parent.fsmagic",     CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_PATH_OK },
	{ "path.parent.gid",         CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_PATH_OK },
	{ "path.parent.ino",         CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_PATH_OK },
	{ "path.parent.major",       CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_PATH_OK },
	{ "path.parent.minor",       CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_PATH_OK },
	{ "path.parent.perm",        CCS_TYPE_FILEPERM, CCS_TYPE_FILEPERM,
	  CCS_PATH_OK },
	{ "path.parent.uid",         CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_PATH_OK },
	{ "path.perm",               CCS_TYPE_FILEPERM, CCS_TYPE_FILEPERM,
	  CCS_PATH_SELF_OK },
	{ "path.type",               CCS_TYPE_FILETYPE, CCS_TYPE_FILETYPE,
	  CCS_PATH_SELF_OK },
	{ "path.uid",                CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_PATH_SELF_OK },
	{ "perm",                    CCS_TYPE_FILEPERM, CCS_TYPE_FILEPERM,
	  CCS_PATH_PERM_OK },
	{ "port",                    CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_IP_SOCKET_OK },
	{ "proto",                   CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_RAW_SOCKET_OK },
	{ "put_old",                 CCS_TYPE_STRING,   CCS_TYPE_STRING,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "put_old.dev_major",       CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "put_old.dev_minor",       CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "put_old.fsmagic",         CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "put_old.gid",             CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "put_old.ino",             CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "put_old.major",           CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "put_old.minor",           CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "put_old.parent.fsmagic",  CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "put_old.parent.gid",      CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "put_old.parent.ino",      CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "put_old.parent.major",    CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "put_old.parent.minor",    CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "put_old.parent.perm",     CCS_TYPE_FILEPERM, CCS_TYPE_FILEPERM,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "put_old.parent.uid",      CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "put_old.perm",            CCS_TYPE_FILEPERM, CCS_TYPE_FILEPERM,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "put_old.type",            CCS_TYPE_FILETYPE, CCS_TYPE_FILETYPE,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "put_old.uid",             CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_PIVOT_ROOT) },
	{ "setgid",                  CCS_TYPE_INVALID,  CCS_TYPE_FILEPERM,
	  CCS_ALL_OK },
	{ "setuid",                  CCS_TYPE_INVALID,  CCS_TYPE_FILEPERM,
	  CCS_ALL_OK },
	{ "sig",                     CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_SIGNAL) },
	{ "socket",                  CCS_TYPE_INVALID,  CCS_TYPE_FILETYPE,
	  CCS_ALL_OK },
	{ "source",                  CCS_TYPE_STRING,   CCS_TYPE_STRING,
	  F(CCS_MAC_MOUNT) },
	{ "source.dev_major",        CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "source.dev_minor",        CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "source.fsmagic",          CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "source.gid",              CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "source.ino",              CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "source.major",            CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "source.minor",            CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "source.parent.fsmagic",   CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "source.parent.gid",       CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "source.parent.ino",       CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "source.parent.major",     CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "source.parent.minor",     CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "source.parent.perm",      CCS_TYPE_FILEPERM, CCS_TYPE_FILEPERM,
	  F(CCS_MAC_MOUNT) },
	{ "source.parent.uid",       CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "source.perm",             CCS_TYPE_FILEPERM, CCS_TYPE_FILEPERM,
	  F(CCS_MAC_MOUNT) },
	{ "source.type",             CCS_TYPE_FILETYPE, CCS_TYPE_FILETYPE,
	  F(CCS_MAC_MOUNT) },
	{ "source.uid",              CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "sticky",                  CCS_TYPE_INVALID,  CCS_TYPE_FILEPERM,
	  CCS_ALL_OK },
	{ "symlink",                 CCS_TYPE_INVALID,  CCS_TYPE_FILETYPE,
	  CCS_ALL_OK },
	{ "target",                  CCS_TYPE_STRING,   CCS_TYPE_STRING,
	  F(CCS_MAC_MOUNT) | F(CCS_MAC_SYMLINK) },
	{ "target.dev_major",        CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "target.dev_minor",        CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "target.fsmagic",          CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "target.gid",              CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "target.ino",              CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "target.major",            CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "target.minor",            CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "target.parent.fsmagic",   CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "target.parent.gid",       CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "target.parent.ino",       CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "target.parent.major",     CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "target.parent.minor",     CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "target.parent.perm",      CCS_TYPE_FILEPERM, CCS_TYPE_FILEPERM,
	  F(CCS_MAC_MOUNT) },
	{ "target.parent.uid",       CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "target.perm",             CCS_TYPE_FILEPERM, CCS_TYPE_FILEPERM,
	  F(CCS_MAC_MOUNT) },
	{ "target.type",             CCS_TYPE_FILETYPE, CCS_TYPE_FILETYPE,
	  F(CCS_MAC_MOUNT) },
	{ "target.uid",              CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_MOUNT) },
	{ "task.domain",             CCS_TYPE_STRING,   CCS_TYPE_STRING,
	  CCS_ALL_OK },
	{ "task.egid",               CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_ALL_OK },
	{ "task.euid",               CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_ALL_OK },
	{ "task.exe",                CCS_TYPE_STRING,   CCS_TYPE_STRING,
	  CCS_ALL_OK },
	{ "task.fsgid",              CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_ALL_OK },
	{ "task.fsuid",              CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_ALL_OK },
	{ "task.gid",                CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_ALL_OK },
	{ "task.pid",                CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_ALL_OK },
	{ "task.ppid",               CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_ALL_OK },
	{ "task.sgid",               CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_ALL_OK },
	{ "task.suid",               CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_ALL_OK },
	{ "task.type",               CCS_TYPE_TASKTYPE, CCS_TYPE_INVALID,
	  CCS_ALL_OK },
	{ "task.uid",                CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  CCS_ALL_OK },
	{ "transition",              CCS_TYPE_ASSIGN,   CCS_TYPE_INVALID,
	  F(CCS_MAC_EXECUTE) | F(CCS_MAC_AUTO_DOMAIN_TRANSITION) |
	  CCS_INET_SOCKET_OK | CCS_UNIX_SOCKET_OK },
	{ "uid",                     CCS_TYPE_NUMBER,   CCS_TYPE_NUMBER,
	  F(CCS_MAC_CHOWN) },
	{ "value",                   CCS_TYPE_STRING,   CCS_TYPE_STRING,
	  F(CCS_MAC_ENVIRON) },
	{ "argv[0]",                 CCS_TYPE_STRING,   CCS_TYPE_INVALID,
	  CCS_EXECUTE_OR_ENVIRON_OK },
	{ "argv[4294967295]",        CCS_TYPE_STRING,   CCS_TYPE_INVALID,
	  CCS_EXECUTE_OR_ENVIRON_OK },
	{ "envp[\"PATH\"]",          CCS_TYPE_STRING,   CCS_TYPE_INVALID,
	  CCS_EXECUTE_OR_ENVIRON_OK },
	{ "envp[\"\\*\"]",           CCS_TYPE_STRING,   CCS_TYPE_INVALID,
	  CCS_EXECUTE_OR_ENVIRON_OK },
	{ "NULL",                    CCS_TYPE_INVALID,  CCS_TYPE_STRING,
	  CCS_ALL_OK },
	{ "\"word\"",                CCS_TYPE_INVALID,  CCS_TYPE_ASSIGN,
	  CCS_ALL_OK },
	{ "\"/\"",                   CCS_TYPE_INVALID,  CCS_TYPE_STRING,
	  CCS_ALL_OK },
	{ "\"/\\*\"",                CCS_TYPE_INVALID,  CCS_TYPE_STRING,
	  CCS_ALL_OK },
	{ "@STRING_GROUP",           CCS_TYPE_INVALID,  CCS_TYPE_STRING,
	  CCS_ALL_OK },
	{ "0",                       CCS_TYPE_INVALID,  CCS_TYPE_NUMBER,
	  CCS_ALL_OK },
	{ "0-4294967295",            CCS_TYPE_INVALID,  CCS_TYPE_NUMBER,
	  CCS_ALL_OK },
	{ "@NUMBER_GROUP",           CCS_TYPE_INVALID,  CCS_TYPE_NUMBER,
	  CCS_ALL_OK },
	{ "127.0.0.1",               CCS_TYPE_INVALID,  CCS_TYPE_IPADDR,
	  CCS_ALL_OK },
	{ "0.0.0.0-0.0.0.1",         CCS_TYPE_INVALID,  CCS_TYPE_IPADDR,
	  CCS_ALL_OK },
	{ "::1",                     CCS_TYPE_INVALID,  CCS_TYPE_IPADDR,
	  CCS_ALL_OK },
	{ "::-::1",                  CCS_TYPE_INVALID,  CCS_TYPE_IPADDR,
	  CCS_ALL_OK },
	{ "@IP_GROUP",               CCS_TYPE_INVALID,  CCS_TYPE_IPADDR,
	  CCS_ALL_OK },
};

static char *policy = NULL;
static int policy_len = 0;

static inline void truncate_policy(void)
{
	policy_len = 0;
}

static void add_policy(const char *data)
{
	const int len = strlen(data);
	char *tmp = realloc(policy, policy_len + len + 1);
	if (!tmp) {
		fprintf(stderr, "Out of memory\n");
		exit(1);
	}
	policy = tmp;
	memmove(policy + policy_len, data, len + 1);
	policy_len += len;
}

static unsigned int tests_now = 0; 

static void check_policy_written(const _Bool valid)
{
	int fd1 = open(POLDIR "/policy", O_RDWR);
	int fd2 = open(POLDIR "/policy", O_RDWR);
	int len = strlen(policy);
	int ret;
	char *buffer;
	if (fd1 == EOF || fd2 == EOF) {
		printf("%s", policy);
		return;
	}
	ret = write(fd1, policy, len);
	if (ret != len) {
		fprintf(stderr, "Write error (%d, %d)\n", ret, len);
		exit(1);
	}
	buffer = calloc(len + 4096, 1);
	if (!buffer) {
		fprintf(stderr, "Out of memory\n");
		exit(1);
	}
	ret = read(fd1, buffer, len + 4095);
	if (!strstr(buffer, policy) != !valid) {
		fprintf(stderr, "Test %u: FAILED(add) '%s' : '%s' valid=%u\n",
			tests_now - 1, buffer, policy, valid);
		exit(1);
	}
	printf("Test %u: OK '%s' : '%s'\n", tests_now - 1, buffer, policy);
	if (write(fd2, "delete ", 7) != 7 || write(fd2, policy, len) != len) {
		fprintf(stderr, "Write error\n");
		exit(1);
	}
	memset(buffer, 0, len + 1024);
	ret = read(fd2, buffer, len + 1023);
	if (strstr(buffer, policy)) {
		fprintf(stderr, "Test %u: FAILED(remove) '%s' : '%s'\n",
			tests_now - 1, buffer, policy);
		exit(1);
	}
	printf("Test %u: OK '%s' : '%s'\n", tests_now - 1, buffer, policy);
	free(buffer);
	close(fd1);
	close(fd2);
}

static inline void add_policy2(const char *data1, const char *data2)
{
	add_policy(data1);
	add_policy(data2);
}

static inline void add_policy3(const char *data1, const char *data2,
			       const char *data3)
{
	add_policy(data1);
	add_policy(data2);
	add_policy(data3);
}

static _Bool adjust_exception(const int mac, const int p1, const int p2)
{
	if (ccs_conditions[p1].left_type == CCS_TYPE_ASSIGN)
		return !strcmp(ccs_conditions[p2].keyword, "NULL") ||
			!strcmp(ccs_conditions[p2].keyword,"\"/\"");
	if (ccs_conditions[p2].keyword[0] == '@')
		return ccs_conditions[p1].left_type == CCS_TYPE_STRING ||
			ccs_conditions[p1].left_type == CCS_TYPE_NUMBER ||
			ccs_conditions[p1].left_type == CCS_TYPE_IPADDR ||
			ccs_conditions[p1].left_type == CCS_TYPE_FILEPERM;
	if (ccs_conditions[p1].left_type == CCS_TYPE_STRING)
		return !strcmp(ccs_conditions[p2].keyword, "\"word\"");
	if (ccs_conditions[p1].left_type == CCS_TYPE_FILEPERM &&
	    ccs_conditions[p2].right_type == CCS_TYPE_NUMBER)
		return 1;
	return 0;
}		

static unsigned int tests_start = 0; 

static void check_righthand(const int mac, const int p1, const _Bool org_valid)
{
	int p2;
	const enum ccs_var_type type = ccs_conditions[p1].left_type;
	for (p2 = 0; p2 < sizeof(ccs_conditions) / sizeof(ccs_conditions[0]);
	     p2++) {
		_Bool valid = org_valid;
		if (tests_now++ < tests_start)
			continue;
		if (valid) {
			if (!(ccs_conditions[p2].available & F(mac)))
				valid = 0;
			else if (ccs_conditions[p2].right_type != type)
				valid = adjust_exception(mac, p1, p2);
		}
		truncate_policy();
		add_policy3("0 acl ", ccs_mac_keywords[mac], "\n"
			    "    audit 0\n    0 allow");
		add_policy2(" ", ccs_conditions[p1].keyword);
		add_policy2("=", ccs_conditions[p2].keyword);
		if (mac == CCS_MAC_AUTO_DOMAIN_TRANSITION &&
		    strcmp(ccs_conditions[p1].keyword, "transition")) 
			add_policy(" transition=\"word\"");
		add_policy("\n");
		check_policy_written(valid);
		if (type == CCS_TYPE_ASSIGN)
			valid = 0;
		truncate_policy();
		add_policy3("0 acl ", ccs_mac_keywords[mac], "\n"
			    "    audit 0\n    0 allow");
		add_policy2(" ", ccs_conditions[p1].keyword);
		add_policy2("!=", ccs_conditions[p2].keyword);
		if (mac == CCS_MAC_AUTO_DOMAIN_TRANSITION &&
		    strcmp(ccs_conditions[p1].keyword, "transition")) 
			add_policy(" transition=\"word\"");
		add_policy("\n");
		check_policy_written(valid);
		{
			static time_t last = 0;
			time_t now = time(NULL);
			if (now - last >= (time_t) 10) {
				last = now;
				fprintf(stderr, "Test %u passed\n",
					tests_now - 1);
			}
		}
	}
}

static void reset_policy(void)
{
	FILE *fp2 = fopen(POLDIR "/policy", "r");
	FILE *fp1 = fopen(POLDIR "/policy", "w");
	if (!fp1 || !fp2) {
		fprintf(stderr, " Can't open " POLDIR "/policy\n");
		exit(1);
	}
	while (1) {
		const int c = fgetc(fp2);
		if (c == EOF)
			break;
		fputc(c, fp1);
		if (c == '\n')
			fprintf(fp1, "delete ");
	}
	fclose(fp2);
	fclose(fp1);

	/* Do not leave the init process in stopped state. */
	kill(1, SIGCONT);
	
	/* Undo mount("/", MS_REC|MS_SHARED) made by systemd. */
	mount(NULL, "/", NULL, MS_REC|MS_PRIVATE, NULL);
}

int main(int argc, char *argv[]) {
	int mac;

	reset_policy();
	if (argc > 1)
		tests_start = atoi(argv[1]);
	for (mac = 0; mac < CCS_MAX_MAC_INDEX; mac++) {
		truncate_policy();
		add_policy3("0 acl ", ccs_mac_keywords[mac], "\n"
			    "    audit 0\n");
		check_policy_written(1);
	}
	for (mac = 0; mac < CCS_MAX_MAC_INDEX; mac++) {
		int p1;
		for (p1 = 0;
		     p1 < sizeof(ccs_conditions) / sizeof(ccs_conditions[0]);
		     p1++) {
			_Bool valid = 1;
			if (ccs_conditions[p1].left_type == CCS_TYPE_INVALID ||
			    !(ccs_conditions[p1].available & F(mac)))
				valid = 0;
			check_righthand(mac, p1, valid);
		}
	}
	fprintf(stderr, "Test %u passed\n", tests_now - 1);
	return 0;
}
