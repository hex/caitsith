/*
 * caitsith_audit2cond_test.c
 *
 * Copyright (C) 2012-2013  Tetsuo Handa
 *
 * Version: 0.2   2016/10/05
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License v2 as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 */

#include <errno.h>
#include <fcntl.h>
#include <linux/ip.h>
#include <linux/kdev_t.h>
#include <linux/reboot.h>
#include <netinet/in.h>
#include <poll.h>
#include <pty.h>
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/mount.h>
#include <sys/ptrace.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/timex.h>
#include <sys/types.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#ifndef CLONE_NEWNS
#include <linux/sched.h>
#endif
#ifndef PRIO_PROCESS
#include <sys/resource.h>
#endif
#ifndef MS_REC
#define MS_REC 16384
#endif
#ifndef MS_PRIVATE
#define MS_PRIVATE  (1 << 18)
#endif
int unshare(int flags);
int reboot(int cmd);
struct module;
int init_module(const char *name, struct module *image);
int delete_module(const char *name);
int pivot_root(const char *new_root, const char *put_old);
struct kexec_segment;
static inline long sys_kexec_load(unsigned long entry,
				  unsigned long nr_segments,
				  struct kexec_segment *segments,
				  unsigned long flags)
{
	return (long) syscall(__NR_kexec_load, entry, nr_segments,
			      segments, flags);
}

static char *args[512];
static int audit_fd = EOF;
static int policy_fd = EOF;
static int one = 1;

static const char *make_variable(const char *name, const unsigned int type)
{
	const char *cp = name + strlen(name) + 1;
	if (type == 0)
		return cp;
	if (*cp == '"') {
		/* Name variables. */
		if (type == 1)
			return "NULL";
		if (type == 2)
			return "!@EMPTY_NAME_GROUP";
		//if (type == 3)
		//	return "task.exe"; // How can this match?
	} else if (!strcmp(name, "ip")) {
		/* IP address variables */
		if (type == 1)
			return "!@EMPTY_IP_GROUP";
	} else if (strstr(name, ".type")) {
		/* task.type or path.type */
		/* No variables supported. */
	} else {
		/* Numeric variables */
		const int zero = !strcmp(cp, "0") || !strcmp(cp, "00") ||
			!strcmp(cp, "0x0");
		if (type == 1)
			return "!@EMPTY_NUMBER_GROUP";
		if (type == 2)
			return "!@ZERO" + zero;
		if (type == 3)
			return "!task.uid" + zero;
		if (strstr(name, ".perm")) {
			unsigned int perm = 0;
			sscanf(cp, "%o", &perm);
			if (type == 4)
				return "!setuid" + ((perm & 04000) != 0);
			if (type == 5)
				return "!setgid" + ((perm & 02000) != 0);
			if (type == 6)
				return "!sticky" + ((perm & 01000) != 0);
			if (type == 7)
				return "!owner_read" + ((perm & 0400) != 0);
			if (type == 8)
				return "!owner_write" + ((perm & 0200) != 0);
			if (type == 9)
				return "!owner_execute" + ((perm & 0100) != 0);
			if (type == 10)
				return "!group_read" + ((perm & 040) != 0);
			if (type == 11)
				return "!group_write" + ((perm & 020) != 0);
			if (type == 12)
				return "!group_execute" + ((perm & 010) != 0);
			if (type == 13)
				return "!others_read" + ((perm & 04) != 0);
			if (type == 14)
				return "!others_write" + ((perm & 02) != 0);
			if (type == 15)
				return "!others_execute" + ((perm & 01) != 0);
		}
	}
	return NULL;
}

static int make_policy(const char *action, const unsigned int loop,
		       const int check, const int max)
{
	static char buffer[1048576];
	int pos;
	int i;
	const char *var = "";
	int not_equals = (loop & 1);
	if (max) {
		/* Nothing more to test if there is no more variables. */
		var = make_variable(args[check], loop >> 1);
		if (!var)
			return 0;
		/* NULL has inverse semantics. */
		if (!strcmp(var, "NULL"))
			not_equals = !not_equals;
		/* Handle other inversed conditions. */
		else if (*var == '!') {
			var++;
			not_equals = !not_equals;
		}
	}
	memset(buffer, 0, sizeof(buffer));
	pos = snprintf(buffer, sizeof(buffer) - 1, "0 acl %s task.ppid=%u\n"
		       "\t0 allow", action, getpid());
	for (i = 0; i < max; i++) {
		/* Change only one argument at a time. */
		if (i != check)
			pos += snprintf(buffer + pos, sizeof(buffer) - 1 - pos,
					" %s=%s", args[i],
					args[i] + strlen(args[i]) + 1);
		else
			pos += snprintf(buffer + pos, sizeof(buffer) - 1 - pos,
					" %s%s=%s", args[i],
					not_equals ?  "!" : "", var);
	}
	pos += snprintf(buffer + pos, sizeof(buffer) - 1 - pos, "\n"
			"\t1 deny\n");
	write(policy_fd, buffer, strlen(buffer));
	printf("Expecting %s: %s", (loop & 1) ? "denied" : "allowed",
	       buffer);
	return 1;
}

static int do_test(int (*func) (void))
{
	int error = 0;
	const pid_t pid = fork();
	switch (pid) {
	case 0:
		if (unshare(CLONE_NEWNS)) {
			fprintf(stderr, "***** Can't unshare.\n");
			_exit(1);
		}
		errno = 0;
		_exit(func());
	case -1:
		fprintf(stderr, "***** Can't fork.\n");
		return 1;
	}
	while (waitpid(pid, &error, 0) == EOF && errno == EINTR);
	return WIFEXITED(error) ? WEXITSTATUS(error) : -1;
}

static int check_result(const char *action, int (*func) (void),
			const unsigned int loop)
{
	static char buffer[1048576];
	char *cp1;
	char *cp2;
	int retries = 0;
retry:
	do_test(func);
	memset(buffer, 0, sizeof(buffer));
	read(audit_fd, buffer, sizeof(buffer) - 1);
	cp1 = strchr(buffer, '\n');
	if (!cp1) {
		if (retries++ < 100)
			goto retry;
		fprintf(stderr, "***** Missing audit log for '%s': %s\n",
			action, buffer);
		return 1;
	}
	*cp1 = '\0';
	cp1 = strstr(buffer, " / ");
	if (!cp1) {
		fprintf(stderr, "***** Corrupted audit log for '%s': %s\n",
			action, buffer);
		return 1;
	}
	cp1 += 3;
	cp2 = strchr(cp1, ' ');
	if (!cp2) {
		fprintf(stderr, "***** Corrupted audit log for '%s': %s\n",
			action, buffer);
		return 1;
	}
	*cp2 = '\0';
	if (strcmp(cp1, action)) {
		if (retries++ < 100)
			goto retry;
		*cp2 = ' ';
		fprintf(stderr, "***** Unexpected audit log for '%s': %s\n",
			action, buffer);
		return 1;
	}
	*cp2 = ' ';
	if (!(loop & 1)) {
		if (!strstr(buffer, " result=allowed ")) {
			fprintf(stderr, "***** result=allowed expected: %s\n",
				buffer);
			exit(1);
			return 1;
		}
	} else {
		if (!strstr(buffer, " result=denied ")) {
			fprintf(stderr, "***** result=denied expected: %s\n",
				buffer);
			exit(1);
			return 1;
		}
	}
	snprintf(buffer, sizeof(buffer) - 1, "delete 0 acl %s task.ppid=%u\n",
		 action, getpid());
	write(policy_fd, buffer, strlen(buffer));
	while (read(audit_fd, buffer, sizeof(buffer) - 1) > 0);
	return 0;
}

static void test_action(const char *action, int (*func) (void))
{
	static char buffer[1048576];
	int pos;
	int i;
	char *cp1;
	char *cp2;
	int retries = 0;
	memset(args, 0, sizeof(args));
	make_policy(action, 0, 0, 0);
retry:
	do_test(func);
	memset(buffer, 0, sizeof(buffer));
	read(audit_fd, buffer, sizeof(buffer) - 1);
	cp1 = strchr(buffer, '\n');
	if (!cp1) {
		if (retries++ < 100)
			goto retry;
		fprintf(stderr, "+++++ Missing audit log for '%s': %s\n",
			action, buffer);
		return;
	}
	*cp1 = '\0';
	cp1 = strstr(buffer, " / ");
	if (!cp1) {
		fprintf(stderr, "+++++ Corrupted audit log for '%s': %s\n",
			action, buffer);
		return;
	}
	cp1 += 3;
	cp2 = strchr(cp1, ' ');
	if (!cp2) {
		fprintf(stderr, "+++++ Corrupted audit log for '%s': %s\n",
			action, buffer);
		return;
	}
	*cp2++ = '\0';
	if (strcmp(cp1, action)) {
		if (retries++ < 100)
			goto retry;
		fprintf(stderr, "+++++ Unexpected audit log for '%s': %s\n",
			action, cp1);
		return;
	}
	cp1 = cp2;
	pos = 0;
	while (pos < (sizeof(args) / sizeof(args[0]))) {
		char *cp3;
		args[pos++] = cp1;
		cp2 = strchr(cp1, ' ');
		if (cp2)
			*cp2++ = '\0';
		cp3 = strchr(cp1, '=');
		if (!cp3 || !*(cp3 + 1)) {
			fprintf(stderr, "+++++ Corrupted audit log.\n");
			return;
		}
		*cp3 = '\0';
		/*
		 * Ignore task.pid which cannot be matched due to tesing under
		 * fork()ed process.
		 */
		if (!strcmp(cp1, "task.pid"))
			pos--;
		/*
		 * Ignore .ino which might change for each test.
		 */
		else if (strstr(cp1, ".ino"))
			pos--;
		/*
		 * Ignore .minor which might change for each test.
		 */
		else if (strstr(cp1, ".minor"))
			pos--;
		cp1 = cp2;
		if (!cp1)
			break;
	}
	if (pos == (sizeof(args) / sizeof(args[0]))) {
		fprintf(stderr, "+++++ Line too long.\n");
		return;
	}
	{
		static char buffer2[1024];
		while (read(audit_fd, buffer2, sizeof(buffer2)) > 0);
	}
	for (i = 0; i < pos; i++) {
		unsigned int loop;
		for (loop = 0; make_policy(action, loop, i, pos); loop++)
			check_result(action, func, loop);
	}
	return;
}

static void startup(void)
{
	int pipe_fd[2] = { EOF, EOF };
	static char buffer[1048576];
	FILE *fp = fopen(POLDIR "/policy", "r");
	policy_fd = open(POLDIR "/policy", O_WRONLY);
	audit_fd = open(POLDIR "/audit", O_RDONLY);
	if (!fp || policy_fd == EOF || audit_fd == EOF) {
		fprintf(stderr,
			"***** Can't open " POLDIR "/ interface.\n");
		exit(1);
	}
	if (pipe(pipe_fd)) {
		fprintf(stderr, "***** Can't pipe.\n");
		exit(1);
	}
	mkdir("/tmp/caitsith.tmp", 0700);
	if (chown("/tmp/caitsith.tmp", 0, 0) ||
	    chmod("/tmp/caitsith.tmp", 0700)) {
		fprintf(stderr, "***** Can't chown/chmod.\n");
		exit(1);
	}
	while (memset(buffer, 0, sizeof(buffer)),
	       fgets(buffer, sizeof(buffer) - 1, fp)) {
		if (!strchr(buffer, '\n')) {
			fprintf(stderr, "***** Line too long.\n");
			exit(1);
		}
		write(policy_fd, "delete ", 7);
		write(policy_fd, buffer, strlen(buffer));
	}
	fclose(fp);
	{
		const char *config = "quota Memory used by audit: 1048576\n"
			"quota audit[0] allowed=1024 denied=1024 "
			"unmatched=1024\n" "number_group ZERO 0\n";
		write(policy_fd, config, strlen(config));
	}
	while (read(audit_fd, buffer, sizeof(buffer)) > 0);
	switch (fork()) {
		int fd;
	case 0:
		close(policy_fd);
		close(audit_fd);
		close(pipe_fd[1]);
		fd = open(POLDIR "/query", O_RDWR);
		while (1) {
			unsigned int serial;
			unsigned int retry;
			struct pollfd pfd[2] = {
				{ fd, POLLIN, 0 },
				{ pipe_fd[0], POLLIN, 0 }
			};
			poll(pfd, 2, -1);
			if (pfd[0].revents & POLLIN)
				break;
			if (read(fd, buffer, sizeof(buffer) - 1) <= 0)
				continue;
			if (sscanf(buffer, "Q%u-%u", &serial, &retry) != 2) {
				fprintf(stderr, "***** Corrupted query: %s\n",
					buffer);
				break;
			}
			snprintf(buffer, sizeof(buffer) - 1, "A%u=%u\n",
				 serial,
				 retry < 5 ? 3 /* Retry */ : 2 /* No */);
			write(fd, buffer, strlen(buffer));
		}
		_exit(0);
	case -1:
		fprintf(stderr, "***** Can't fork.\n");
		exit(1);
	}
	close(pipe_fd[0]);
}

static int test_execute(void)
{
	execlp(BINDIR "/true", "true", NULL);
	return errno;
}

static int test_read(void)
{
	open("/dev/null", O_RDONLY);
	return errno;
}

static int test_write(void)
{
	open("/dev/null", O_WRONLY);
	return errno;
}

static int test_create_no_append(void)
{
	int err;
	open("/tmp/caitsith.tmp/file", O_CREAT | O_RDWR | O_TRUNC, 0600);
	err = errno;
	unlink("/tmp/caitsith.tmp/file");
	return err;
}

static int test_create_append(void)
{
	int err;
	open("/tmp/caitsith.tmp/file", O_CREAT | O_RDWR | O_TRUNC | O_APPEND,
	     0600);
	err = errno;
	unlink("/tmp/caitsith.tmp/file");
	return err;
}

static int test_fcntl_clear_append(void)
{
	const int fd = open("/dev/null", O_WRONLY | O_APPEND);
	const int flags = fcntl(fd, F_GETFL);
	errno = 0;
	fcntl(fd, F_SETFL, flags & ~O_APPEND);
	return errno;
}

static int test_fcntl_set_append(void)
{
	const int fd = open("/dev/null", O_WRONLY);
	const int flags = fcntl(fd, F_GETFL);
	errno = 0;
	fcntl(fd, F_SETFL, flags | O_APPEND);
	return errno;
}

static int test_unlink(void)
{
	mknod("/tmp/caitsith.tmp/file", S_IFREG | 0600, 0);
	errno = 0;
	unlink("/tmp/caitsith.tmp/file");
	return errno;
}

static int test_getattr(void)
{
	struct stat buf;
	stat("/dev/null", &buf);
	return errno;
}

static int test_mkdir(void)
{
	int err;
	mkdir("/tmp/caitsith.tmp/dir", 0755);
	err = errno;
	rmdir("/tmp/caitsith.tmp/dir");
	return err;
}

static int test_rmdir(void)
{
	mkdir("/tmp/caitsith.tmp/dir", 0755);
	errno = 0;
	rmdir("/tmp/caitsith.tmp/dir");
	return errno;
}

static int test_mkfifo(void)
{
	int err;
	mknod("/tmp/caitsith.tmp/fifo", S_IFIFO | 0600, 0);
	err = errno;
	unlink("/tmp/caitsith.tmp/fifo");
	return err;
}

static int test_mksock_by_mknod(void)
{
	int err;
	mknod("/tmp/caitsith.tmp/sock", S_IFSOCK | 0600, 0);
	err = errno;
	unlink("/tmp/caitsith.tmp/sock");
	return err;
}

static int test_mksock_by_bind(void)
{
	struct sockaddr_un addr;
	int err;
	int fd = socket(PF_UNIX, SOCK_STREAM, 0);
	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	snprintf(addr.sun_path, sizeof(addr.sun_path) - 1,
		 "/tmp/caitsith.tmp/sock");
	errno = 0;
	bind(fd, (struct sockaddr *) &addr, sizeof(addr));
	err = errno;
	unlink(addr.sun_path);
	return err;
}

static int test_truncate(void)
{
	mknod("/tmp/caitsith.tmp/truncate", S_IFREG | 0600, 0);
	errno = 0;
	truncate("/tmp/caitsith.tmp/truncate", 0);
	return errno;
}

static int test_symlink(void)
{
	int err;
	symlink(".", "/tmp/caitsith.tmp/symlink");
	err = errno;
	unlink("/tmp/caitsith.tmp/symlink");
	return err;
}

static int test_mkchar(void)
{
	int err;
	mknod("/tmp/caitsith.tmp/char", S_IFCHR | 0600, MKDEV(1, 3));
	err = errno;
	unlink("/tmp/caitsith.tmp/char");
	return err;
}

static int test_mkblock(void)
{
	int err;
	mknod("/tmp/caitsith.tmp/block", S_IFBLK | 0600, MKDEV(1, 0));
	err = errno;
	unlink("/tmp/caitsith.tmp/block");
	return err;
}

static int test_link(void)
{
	int err;
	mknod("/tmp/caitsith.tmp/old_path", S_IFREG | 0600, 0);
	errno = 0;
	link("/tmp/caitsith.tmp/old_path", "/tmp/caitsith.tmp/new_path");
	err = errno;
	unlink("/tmp/caitsith.tmp/old_path");
	unlink("/tmp/caitsith.tmp/new_path");
	return err;
}

static int test_rename(void)
{
	int err;
	mknod("/tmp/caitsith.tmp/old_path", S_IFREG | 0600, 0);
	errno = 0;
	rename("/tmp/caitsith.tmp/old_path", "/tmp/caitsith.tmp/new_path");
	err = errno;
	unlink("/tmp/caitsith.tmp/old_path");
	unlink("/tmp/caitsith.tmp/new_path");
	return err;
}

static int test_chmod(void)
{
	chmod("/dev/null", 0666);
	return errno;
}

static int test_chown(void)
{
	chown("/dev/null", 0, -1);
	return errno;
}

static int test_chgrp(void)
{
	chown("/dev/null", -1, 0);
	return errno;
}

static int test_ioctl(void)
{
	const int fd = open("/dev/null", O_WRONLY);
	errno = 0;
	ioctl(fd, 0, 0);
	return errno;
}

static int test_chroot(void)
{
	chroot("/tmp/caitsith.tmp");
	return errno;
}

static int test_mount(void)
{
	mount(NULL, "/tmp/caitsith.tmp", "tmpfs", 0, NULL);
	return errno;
}

static int test_unmount(void)
{
	mount(NULL, "/tmp/caitsith.tmp", "tmpfs", 0, NULL);
	errno = 0;
	umount("/tmp/caitsith.tmp");
	return errno;
}

static int test_pivot_root(void)
{
	errno = 0;
	pivot_root("/sys/kernel/security/", "/sys/kernel/security/caitsith/");
	if (errno == ENOENT) {
		errno = 0;
		pivot_root("/proc/", "/proc/caitsith/");
	}
	return errno;
}

static void inet_bind(const int fd, const unsigned short int port)
{
	struct sockaddr_in addr = { };
	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
	addr.sin_port = htons(port);
	bind(fd, (struct sockaddr *) &addr, sizeof(addr));
}

static int test_inet_stream_bind(void)
{
	int fd = socket(PF_INET, SOCK_STREAM, 0);
	struct sockaddr_in addr = { };
	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
	addr.sin_port = htons(10000);
	errno = 0;
	bind(fd, (struct sockaddr *) &addr, sizeof(addr));
	return errno;
}

static int test_inet_stream_listen(void)
{
	int fd = socket(PF_INET, SOCK_STREAM, 0);
	inet_bind(fd, 10000);
	errno = 0;
	listen(fd, 5);
	return errno;
}

static int test_inet_stream_connect(void)
{
	int fd1 = socket(PF_INET, SOCK_STREAM, 0);
	int fd2 = socket(PF_INET, SOCK_STREAM, 0);
	struct sockaddr_in addr = { };
	socklen_t size = sizeof(addr);
	inet_bind(fd1, 10000);
	listen(fd1, 0);
	inet_bind(fd2, 10001);
	getsockname(fd1, (struct sockaddr *) &addr, &size);
	errno = 0;
	connect(fd2, (struct sockaddr *) &addr, sizeof(addr));
	return errno;
}

static int test_inet_stream_accept(void)
{
	int fd1 = socket(PF_INET, SOCK_STREAM, 0);
	int fd2 = socket(PF_INET, SOCK_STREAM, 0);
	struct sockaddr_in addr = { };
	socklen_t size = sizeof(addr);
	inet_bind(fd1, 10000);
	listen(fd1, 0);
	inet_bind(fd2, 10001);
	getsockname(fd1, (struct sockaddr *) &addr, &size);
	connect(fd2, (struct sockaddr *) &addr, sizeof(addr));
	errno = 0;
	accept(fd1, (struct sockaddr *) &addr, &size);
	return errno;
}

static int test_inet_dgram_bind(void)
{
	int fd = socket(PF_INET, SOCK_DGRAM, 0);
	struct sockaddr_in addr = { };
	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
	addr.sin_port = htons(10000);
	errno = 0;
	bind(fd, (struct sockaddr *) &addr, sizeof(addr));
	return errno;
}

static int test_inet_dgram_connect(void)
{
	int fd1 = socket(PF_INET, SOCK_DGRAM, 0);
	int fd2 = socket(PF_INET, SOCK_DGRAM, 0);
	struct sockaddr_in addr = { };
	socklen_t size = sizeof(addr);
	inet_bind(fd1, 10000);
	inet_bind(fd2, 10001);
	getsockname(fd1, (struct sockaddr *) &addr, &size);
	errno = 0;
	connect(fd2, (struct sockaddr *) &addr, sizeof(addr));
	return errno;
}

static int test_inet_dgram_send(void)
{
	int fd1 = socket(PF_INET, SOCK_DGRAM, 0);
	int fd2 = socket(PF_INET, SOCK_DGRAM, 0);
	struct sockaddr_in addr = { };
	socklen_t size = sizeof(addr);
	inet_bind(fd1, 10000);
	inet_bind(fd2, 10001);
	getsockname(fd1, (struct sockaddr *) &addr, &size);
	errno = 0;
	sendto(fd2, "", 1, 0, (struct sockaddr *) &addr, sizeof(addr));
	return errno;
}

static int test_inet_dgram_recv(void)
{
	int fd1 = socket(PF_INET, SOCK_DGRAM, 0);
	int fd2 = socket(PF_INET, SOCK_DGRAM, 0);
	char c;
	struct sockaddr_in addr = { };
	socklen_t size = sizeof(addr);
	inet_bind(fd1, 10000);
	inet_bind(fd2, 10001);
	getsockname(fd1, (struct sockaddr *) &addr, &size);
	sendto(fd2, "", 1, 0, (struct sockaddr *) &addr, sizeof(addr));
	errno = 0;
	recvfrom(fd1, &c, 1, MSG_DONTWAIT, (struct sockaddr *) &addr, &size);
	return errno;
}

static int test_inet_raw_bind(void)
{
	int fd = socket(PF_INET, SOCK_RAW, IPPROTO_RAW);
	struct sockaddr_in addr = { };
	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
	addr.sin_port = htons(10000);
	errno = 0;
	bind(fd, (struct sockaddr *) &addr, sizeof(addr));
	return errno;
}

static int test_inet_raw_connect(void)
{
	int fd1 = socket(PF_INET, SOCK_RAW, IPPROTO_RAW);
	int fd2 = socket(PF_INET, SOCK_RAW, IPPROTO_RAW);
	struct sockaddr_in addr = { };
	socklen_t size = sizeof(addr);
	inet_bind(fd1, 10000);
	inet_bind(fd2, 10001);
	getsockname(fd1, (struct sockaddr *) &addr, &size);
	errno = 0;
	connect(fd2, (struct sockaddr *) &addr, sizeof(addr));
	return errno;
}

static int test_inet_raw_send(void)
{
	int fd1 = socket(PF_INET, SOCK_RAW, IPPROTO_RAW);
	int fd2 = socket(PF_INET, SOCK_RAW, IPPROTO_RAW);
	struct sockaddr_in addr = { };
	socklen_t size = sizeof(addr);
	static struct iphdr ip = { };
	inet_bind(fd1, 10000);
	inet_bind(fd2, 10001);
	getsockname(fd1, (struct sockaddr *) &addr, &size);
	ip.version = 4;
	ip.ihl = sizeof(struct iphdr) / 4;
	ip.protocol = IPPROTO_RAW;
	ip.daddr = htonl(INADDR_LOOPBACK);
	ip.saddr = ip.daddr;
	errno = 0;
	sendto(fd2, &ip, sizeof(ip), 0, (struct sockaddr *) &addr,
	       sizeof(addr));
	return errno;
}

static int test_inet_raw_recv(void)
{
	int fd1 = socket(PF_INET, SOCK_RAW, IPPROTO_RAW);
	int fd2 = socket(PF_INET, SOCK_RAW, IPPROTO_RAW);
	struct sockaddr_in addr = { };
	socklen_t size = sizeof(addr);
	static struct iphdr ip = { };
	inet_bind(fd1, 10000);
	inet_bind(fd2, 10000);
	getsockname(fd1, (struct sockaddr *) &addr, &size);
	ip.version = 4;
	ip.ihl = sizeof(struct iphdr) / 4;
	ip.protocol = IPPROTO_RAW;
	ip.daddr = htonl(INADDR_LOOPBACK);
	ip.saddr = ip.daddr;
	sendto(fd2, &ip, sizeof(ip), 0, (struct sockaddr *) &addr,
	       sizeof(addr));
	errno = 0;
	recvfrom(fd1, &ip, sizeof(ip), MSG_DONTWAIT, (struct sockaddr *) &addr,
		 &size);
	return errno;
}

static void unix_bind(const int fd, const _Bool listener)
{
	struct sockaddr_un addr = { };
	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one));
	addr.sun_family = AF_UNIX;
	snprintf(addr.sun_path, sizeof(addr.sun_path) - 1,
		 "/tmp/caitsith.tmp/%s", listener ? "server" : "client");
	unlink(addr.sun_path);
	bind(fd, (struct sockaddr *) &addr, sizeof(addr));
}

static int test_unix_stream_bind(void)
{
	int fd = socket(PF_UNIX, SOCK_STREAM, 0);
	struct sockaddr_un addr = { };
	addr.sun_family = AF_UNIX;
	errno = 0;
	bind(fd, (struct sockaddr *) &addr, sizeof(addr.sun_family));
	return errno;
}

static int test_unix_stream_listen(void)
{
	int fd = socket(PF_UNIX, SOCK_STREAM, 0);
	unix_bind(fd, 1);
	errno = 0;
	listen(fd, 5);
	return errno;
}

static int test_unix_stream_connect(void)
{
	int fd1 = socket(PF_UNIX, SOCK_STREAM, 0);
	int fd2 = socket(PF_UNIX, SOCK_STREAM, 0);
	struct sockaddr_un addr = { };
	socklen_t size = sizeof(addr);
	unix_bind(fd1, 1);
	unix_bind(fd2, 0);
	listen(fd1, 0);
	getsockname(fd1, (struct sockaddr *) &addr, &size);
	errno = 0;
	connect(fd2, (struct sockaddr *) &addr, sizeof(addr));
	return errno;
}

static int test_unix_stream_accept(void)
{
	int fd1 = socket(PF_UNIX, SOCK_STREAM, 0);
	int fd2 = socket(PF_UNIX, SOCK_STREAM, 0);
	struct sockaddr_un addr = { };
	socklen_t size = sizeof(addr);
	unix_bind(fd1, 1);
	unix_bind(fd2, 0);
	listen(fd1, 0);
	getsockname(fd1, (struct sockaddr *) &addr, &size);
	connect(fd2, (struct sockaddr *) &addr, sizeof(addr));
	errno = 0;
	accept(fd1, (struct sockaddr *) &addr, &size);
	return errno;
}

static int test_unix_dgram_bind(void)
{
	int fd = socket(PF_UNIX, SOCK_DGRAM, 0);
	int err;
	struct sockaddr_un addr = { };
	addr.sun_family = AF_UNIX;
	errno = 0;
	bind(fd, (struct sockaddr *) &addr, sizeof(addr));
	err = errno;
	close(fd);
	return err;
}

static int test_unix_dgram_connect(void)
{
	int fd1 = socket(PF_UNIX, SOCK_DGRAM, 0);
	int fd2 = socket(PF_UNIX, SOCK_DGRAM, 0);
	int err;
	struct sockaddr_un addr = { };
	socklen_t size = sizeof(addr);
	unix_bind(fd1, 1);
	unix_bind(fd2, 0);
	getsockname(fd1, (struct sockaddr *) &addr, &size);
	errno = 0;
	connect(fd2, (struct sockaddr *) &addr, sizeof(addr));
	err = errno;
	close(fd1);
	close(fd2);
	return err;
}

static int test_unix_dgram_send(void)
{
	int fd1 = socket(PF_UNIX, SOCK_DGRAM, 0);
	int fd2 = socket(PF_UNIX, SOCK_DGRAM, 0);
	int err;
	struct sockaddr_un addr = { };
	socklen_t size = sizeof(addr);
	unix_bind(fd1, 1);
	unix_bind(fd2, 0);
	getsockname(fd1, (struct sockaddr *) &addr, &size);
	errno = 0;
	sendto(fd2, "", 1, 0, (struct sockaddr *) &addr, sizeof(addr));
	err = errno;
	close(fd1);
	close(fd2);
	return err;
}

static int test_unix_dgram_recv(void)
{
	int fd1 = socket(PF_UNIX, SOCK_DGRAM, 0);
	int fd2 = socket(PF_UNIX, SOCK_DGRAM, 0);
	int err;
	char c;
	struct sockaddr_un addr = { };
	socklen_t size = sizeof(addr);
	unix_bind(fd1, 1);
	unix_bind(fd2, 0);
	getsockname(fd1, (struct sockaddr *) &addr, &size);
	sendto(fd2, "", 1, 0, (struct sockaddr *) &addr, sizeof(addr));
	errno = 0;
	recvfrom(fd1, &c, 1, MSG_DONTWAIT, (struct sockaddr *) &addr, &size);
	err = errno;
	close(fd1);
	close(fd2);
	return err;
}

static int test_unix_seqpacket_bind(void)
{
	int fd = socket(PF_UNIX, SOCK_SEQPACKET, 0);
	int err;
	struct sockaddr_un addr = { };
	addr.sun_family = AF_UNIX;
	errno = 0;
	bind(fd, (struct sockaddr *) &addr, sizeof(addr.sun_family));
	err = errno;
	close(fd);
	return err;
}

static int test_unix_seqpacket_listen(void)
{
	int fd = socket(PF_UNIX, SOCK_SEQPACKET, 0);
	int err;
	unix_bind(fd, 1);
	errno = 0;
	listen(fd, 5);
	err = errno;
	close(fd);
	return err;
}

static int test_unix_seqpacket_connect(void)
{
	int fd1 = socket(PF_UNIX, SOCK_SEQPACKET, 0);
	int fd2 = socket(PF_UNIX, SOCK_SEQPACKET, 0);
	int err;
	struct sockaddr_un addr = { };
	socklen_t size = sizeof(addr);
	unix_bind(fd1, 1);
	unix_bind(fd2, 0);
	listen(fd1, 0);
	getsockname(fd1, (struct sockaddr *) &addr, &size);
	errno = 0;
	connect(fd2, (struct sockaddr *) &addr, sizeof(addr));
	err = errno;
	close(fd1);
	close(fd2);
	return err;
}

static int test_unix_seqpacket_accept(void)
{
	int fd1 = socket(PF_UNIX, SOCK_SEQPACKET, 0);
	int fd2 = socket(PF_UNIX, SOCK_SEQPACKET, 0);
	struct sockaddr_un addr = { };
	socklen_t size = sizeof(addr);
	unix_bind(fd1, 1);
	unix_bind(fd2, 0);
	listen(fd1, 0);
	getsockname(fd1, (struct sockaddr *) &addr, &size);
	connect(fd2, (struct sockaddr *) &addr, sizeof(addr));
	errno = 0;
	accept(fd1, (struct sockaddr *) &addr, &size);
	return errno;
}

static int test_environ(void)
{
	char * const argv[2] = { "true", NULL };
	char * const envp[2] = { "HOME=/", NULL };
	execve(BINDIR "/true", argv, envp);
	return errno;
}

static int test_ptrace(void)
{
	ptrace(PTRACE_ATTACH, 1, NULL, NULL);
	return errno;
}

static int test_signal(void)
{
	kill(1, 1);
	return errno;
}

static int test_modify_policy(void)
{
	const char *policy = "1 acl modify_policy\n";
	/*
	 * Try to execute a non-executable in order to clear cached policy
	 * manager attribute.
	 */
	const int fd = open("/tmp/caitsith.tmp/exec",
			    O_CREAT | O_TRUNC | O_WRONLY, 0600);
	fchmod(fd, 0700);
	close(fd);
	execl("/tmp/caitsith.tmp/exec", "exec", NULL);
	/* Now policy manager attribute was cleared. */
	errno = 0;
	write(policy_fd, policy, strlen(policy));
	return errno;
}

static int test_use_netlink_socket(void)
{
	const int fd = socket(AF_ROUTE, SOCK_RAW, 0);
	const int err = errno;
	close(fd);
	return err;
}

static int test_use_packet_socket(void)
{
	const int fd = socket(AF_PACKET, SOCK_RAW, 0);
	const int err = errno;
	close(fd);
	return err;
}

static int test_use_reboot(void)
{
	FILE *fp = fopen("/proc/sys/kernel/ctrl-alt-del", "a+");
	unsigned int c;
	int err;
	if (fp && fscanf(fp, "%u", &c) == 1) {
		errno = 0;
		reboot(LINUX_REBOOT_CMD_CAD_ON);
		err = errno;
		rewind(fp);
		fprintf(fp, "%u\n", c);
	} else {
		errno = 0;
		/* Use invalid value */
		reboot(0x0000C0DE);
		err = errno;
	}
	if (fp)
		fclose(fp);
	return err;
}

static int test_use_vhangup(void)
{
	setsid();
	errno = 0;
	vhangup();
	return errno;
}

static int test_set_time_by_stime(void)
{
	const time_t now = time(NULL);
	errno = 0;
	stime(&now);
	return errno;
}

static int test_set_time_by_settimeofday(void)
{
	struct timeval tv;
	struct timezone tz;
	gettimeofday(&tv, &tz);
	errno = 0;
	settimeofday(&tv, &tz);
	return errno;
}

static int test_set_time_by_adjtimex(void)
{
	struct timex buf = { };
	buf.modes = 0x100; /* Use invalid value so that the clock
			      won't change. */
	adjtimex(&buf);
	return errno;
}

static int test_set_priority_by_nice(void)
{
	nice(0);
	return errno;
}

static int test_set_priority_by_setpriority(void)
{
	const int priority = getpriority(PRIO_PROCESS, getpid());
	errno = 0;
	setpriority(PRIO_PROCESS, getpid(), priority);
	return errno;
}

static int test_set_hostname_by_sethostname(void)
{
	char buffer[4096] = { };
	gethostname(buffer, sizeof(buffer) - 1);
	errno = 0;
	sethostname(buffer, strlen(buffer));
	return errno;
}

static int test_set_hostname_by_setdomainname(void)
{
	char buffer[4096] = { };
	getdomainname(buffer, sizeof(buffer) - 1);
	errno = 0;
	setdomainname(buffer, strlen(buffer));
	return errno;
}

static int test_use_kernel_module_by_init_module(void)
{
	init_module("", NULL);
	return errno;
}

static int test_use_kernel_module_by_delete_module(void)
{
	delete_module("");
	return errno;
}

static int test_use_new_kernel(void)
{
	sys_kexec_load(0, 0, NULL, 0);
	return errno;
}

static void reset_policy(void)
{
	FILE *fp2 = fopen(POLDIR "/policy", "r");
	FILE *fp1 = fopen(POLDIR "/policy", "w");
	if (!fp1 || !fp2) {
		fprintf(stderr, " Can't open " POLDIR "/policy\n");
		exit(1);
	}
	while (1) {
		const int c = fgetc(fp2);
		if (c == EOF)
			break;
		fputc(c, fp1);
		if (c == '\n')
			fprintf(fp1, "delete ");
	}
	fclose(fp2);
	fclose(fp1);

	/* Do not leave the init process in stopped state. */
	kill(1, SIGCONT);
	
	/* Undo mount("/", MS_REC|MS_SHARED) made by systemd. */
	mount(NULL, "/", NULL, MS_REC|MS_PRIVATE, NULL);
}

int main(int argc, char *argv[])
{
	int i;
	static const struct {
		const char *action;
		int (*func) (void);
	} testcases[] = {
		{ "execute", test_execute },
		{ "read", test_read },
		{ "write", test_write },
		{ "create", test_create_no_append },
		{ "read", test_create_no_append },
		{ "write", test_create_no_append },
		/* { "truncate", test_create_no_append }, */
		{ "create", test_create_append },
		{ "read", test_create_append },
		{ "append", test_create_append },
		/* { "truncate", test_create_append }, */
		{ "write", test_fcntl_clear_append },
		{ "append", test_fcntl_set_append },
		{ "unlink", test_unlink },
		{ "getattr", test_getattr },
		{ "mkdir", test_mkdir },
		{ "rmdir", test_rmdir },
		{ "mkfifo", test_mkfifo },
		{ "mksock", test_mksock_by_mknod },
		{ "mksock", test_mksock_by_bind },
		{ "truncate", test_truncate },
		{ "symlink", test_symlink },
		{ "mkchar", test_mkchar },
		{ "mkblock", test_mkblock },
		{ "link", test_link },
		{ "rename", test_rename },
		{ "chmod", test_chmod },
		{ "chown", test_chown },
		{ "chgrp", test_chgrp },
		{ "ioctl", test_ioctl },
		{ "chroot", test_chroot },
		{ "mount", test_mount },
		{ "unmount", test_unmount },
		{ "pivot_root", test_pivot_root },
		{ "inet_stream_bind", test_inet_stream_bind },
		{ "inet_stream_listen", test_inet_stream_listen },
		{ "inet_stream_connect", test_inet_stream_connect },
		{ "inet_stream_accept", test_inet_stream_accept },
		{ "inet_dgram_bind", test_inet_dgram_bind },
		{ "inet_dgram_send", test_inet_dgram_connect },
		{ "inet_dgram_send", test_inet_dgram_send },
		{ "inet_dgram_recv", test_inet_dgram_recv },
		{ "inet_raw_bind", test_inet_raw_bind },
		{ "inet_raw_send", test_inet_raw_connect },
		{ "inet_raw_send", test_inet_raw_send },
		{ "inet_raw_recv", test_inet_raw_recv },
		{ "unix_stream_bind", test_unix_stream_bind },
		{ "unix_stream_listen", test_unix_stream_listen },
		{ "unix_stream_connect", test_unix_stream_connect },
		{ "unix_stream_accept", test_unix_stream_accept },
		{ "unix_dgram_bind", test_unix_dgram_bind },
		{ "unix_dgram_send", test_unix_dgram_connect },
		{ "unix_dgram_send", test_unix_dgram_send },
		{ "unix_dgram_recv", test_unix_dgram_recv },
		{ "unix_seqpacket_bind", test_unix_seqpacket_bind },
		{ "unix_seqpacket_listen", test_unix_seqpacket_listen },
		{ "unix_seqpacket_connect", test_unix_seqpacket_connect },
		{ "unix_seqpacket_accept", test_unix_seqpacket_accept },
		{ "environ", test_environ },
		{ "ptrace", test_ptrace },
		{ "signal", test_signal },
		{ "modify_policy", test_modify_policy },
		{ "use_netlink_socket", test_use_netlink_socket },
		{ "use_packet_socket", test_use_packet_socket },
		{ "use_reboot", test_use_reboot },
		{ "use_vhangup", test_use_vhangup },
		{ "set_time", test_set_time_by_stime },
		{ "set_time", test_set_time_by_settimeofday },
		{ "set_time", test_set_time_by_adjtimex },
		{ "set_priority", test_set_priority_by_nice },
		{ "set_priority", test_set_priority_by_setpriority },
		{ "set_hostname", test_set_hostname_by_sethostname },
		{ "set_hostname", test_set_hostname_by_setdomainname },
		{ "use_kernel_module", test_use_kernel_module_by_init_module },
		{ "use_kernel_module",
		  test_use_kernel_module_by_delete_module },
		{ "use_new_kernel", test_use_new_kernel },
		{ NULL, NULL }
	};

	reset_policy();

	startup();
	for (i = 0; testcases[i].action; i++)
		test_action(testcases[i].action, testcases[i].func);
	fprintf(stderr, "Done.\n");
	return 0;
}
