#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define bool _Bool
#define true 1
#define false 0
#define u32 unsigned int
#define u8 unsigned char

struct cs_path_info {
	const char *name;
	u32 hash;          /* = full_name_hash(name, strlen(name)) */
	u32 total_len;     /* = strlen(name)                       */
	u32 const_len;     /* = cs_const_part_length(name)        */
};

static void out_of_memory(void)
{
	fprintf(stderr, "Out of memory\n");
	exit(1);
}

/* Copied from Linux kernel source code. */
static unsigned int full_name_hash(const unsigned char *name, unsigned int len)
{
	unsigned long hash = 0;
	while (len--) {
		unsigned long c = *name++;
		hash = (hash + (c << 4) + (c >> 4)) * 11;
	}
	return (unsigned int) hash;
}

/**
 * cs_pathcmp - strcmp() for "struct cs_path_info" structure.
 *
 * @a: Pointer to "struct cs_path_info".
 * @b: Pointer to "struct cs_path_info".
 *
 * Returns true if @a != @b, false otherwise.
 */
static inline bool cs_pathcmp(const struct cs_path_info *a,
			      const struct cs_path_info *b)
{
	return a->hash != b->hash || strcmp(a->name, b->name);
}

/**
 * cs_const_part_length - Evaluate the initial length without a pattern in a token.
 *
 * @filename: The string to evaluate. Maybe NULL.
 *
 * Returns the initial length without a pattern in @filename.
 */
static int cs_const_part_length(const char *filename)
{
	char c;
	int len = 0;
	if (!filename)
		return 0;
	while (1) {
		c = *filename++;
		if (!c)
			break;
		if (c != '\\') {
			len++;
			continue;
		}
		c = *filename++;
		switch (c) {
		case '0':   /* "\ooo" */
		case '1':
		case '2':
		case '3':
			c = *filename++;
			if (c < '0' || c > '7')
				break;
			c = *filename++;
			if (c < '0' || c > '7')
				break;
			len += 4;
			continue;
		}
		break;
	}
	return len;
}

/**
 * cs_fill_path_info - Fill in "struct cs_path_info" members.
 *
 * @ptr: Pointer to "struct cs_path_info" to fill in.
 *
 * Returns nothing.
 *
 * The caller sets "struct cs_path_info"->name.
 */
static void cs_fill_path_info(struct cs_path_info *ptr)
{
	const char *name = ptr->name;
	const int len = strlen(name);
	ptr->total_len = len;
	ptr->const_len = cs_const_part_length(name);
	ptr->hash = full_name_hash((const unsigned char *) name, len);
}

/**
 * cs_byte_range - Check whether the string is a \ooo style octal value.
 *
 * @str: Pointer to the string.
 *
 * Returns true if @str is a \ooo style octal value, false otherwise.
 */
static bool cs_byte_range(const char *str)
{
	return *str >= '0' && *str++ <= '3' &&
		*str >= '0' && *str++ <= '7' &&
		*str >= '0' && *str <= '7';
}

/**
 * cs_decimal - Check whether the character is a decimal character.
 *
 * @c: The character to check.
 *
 * Returns true if @c is a decimal character, false otherwise.
 */
static bool cs_decimal(const char c)
{
	return c >= '0' && c <= '9';
}

/**
 * cs_hexadecimal - Check whether the character is a hexadecimal character.
 *
 * @c: The character to check.
 *
 * Returns true if @c is a hexadecimal character, false otherwise.
 */
static bool cs_hexadecimal(const char c)
{
	return (c >= '0' && c <= '9') ||
		(c >= 'A' && c <= 'F') ||
		(c >= 'a' && c <= 'f');
}

/**
 * cs_alphabet_char - Check whether the character is an alphabet.
 *
 * @c: The character to check.
 *
 * Returns true if @c is an alphabet character, false otherwise.
 */
static bool cs_alphabet_char(const char c)
{
	return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
}

/**
 * cs_file_matches_pattern2 - Pattern matching without '/' character and "\-" pattern.
 *
 * @filename:     The start of string to check.
 * @filename_end: The end of string to check.
 * @pattern:      The start of pattern to compare.
 * @pattern_end:  The end of pattern to compare.
 *
 * Returns true if @filename matches @pattern, false otherwise.
 */
static bool cs_file_matches_pattern2(const char *filename,
				     const char *filename_end,
				     const char *pattern,
				     const char *pattern_end)
{
	while (filename < filename_end && pattern < pattern_end) {
		char c;
		if (*pattern != '\\') {
			if (*filename++ != *pattern++)
				return false;
			continue;
		}
		c = *filename;
		pattern++;
		switch (*pattern) {
			int i;
			int j;
		case '?':
			if (c == '/') {
				return false;
			} else if (c == '\\') {
				if (cs_byte_range(filename + 1))
					filename += 3;
				else
					return false;
			}
			break;
		case '+':
			if (!cs_decimal(c))
				return false;
			break;
		case 'x':
			if (!cs_hexadecimal(c))
				return false;
			break;
		case 'a':
			if (!cs_alphabet_char(c))
				return false;
			break;
		case '0':
		case '1':
		case '2':
		case '3':
			if (c == '\\' && cs_byte_range(filename + 1)
			    && !strncmp(filename + 1, pattern, 3)) {
				filename += 3;
				pattern += 2;
				break;
			}
			return false; /* Not matched. */
		case '*':
		case '@':
			for (i = 0; i <= filename_end - filename; i++) {
				if (cs_file_matches_pattern2(filename + i,
							     filename_end,
							     pattern + 1,
							     pattern_end))
					return true;
				c = filename[i];
				if (c == '.' && *pattern == '@')
					break;
				if (c != '\\')
					continue;
				if (cs_byte_range(filename + i + 1))
					i += 3;
				else
					break; /* Bad pattern. */
			}
			return false; /* Not matched. */
		default:
			j = 0;
			c = *pattern;
			if (c == '$') {
				while (cs_decimal(filename[j]))
					j++;
			} else if (c == 'X') {
				while (cs_hexadecimal(filename[j]))
					j++;
			} else if (c == 'A') {
				while (cs_alphabet_char(filename[j]))
					j++;
			}
			for (i = 1; i <= j; i++) {
				if (cs_file_matches_pattern2(filename + i,
							     filename_end,
							     pattern + 1,
							     pattern_end))
					return true;
			}
			return false; /* Not matched or bad pattern. */
		}
		filename++;
		pattern++;
	}
	/* Ignore trailing "\*" and "\@" in @pattern. */
	while (*pattern == '\\' &&
	       (*(pattern + 1) == '*' || *(pattern + 1) == '@'))
		pattern += 2;
	return filename == filename_end && pattern == pattern_end;
}

/**
 * cs_file_matches_pattern - Pattern matching without '/' character.
 *
 * @filename:     The start of string to check.
 * @filename_end: The end of string to check.
 * @pattern:      The start of pattern to compare.
 * @pattern_end:  The end of pattern to compare.
 *
 * Returns true if @filename matches @pattern, false otherwise.
 */
static bool cs_file_matches_pattern(const char *filename,
				    const char *filename_end,
				    const char *pattern,
				    const char *pattern_end)
{
	const char *pattern_start = pattern;
	bool first = true;
	bool result;
	if (filename_end > filename && memchr(filename, '/', filename_end - filename)) {
		printf("'");
		fwrite(filename, 1, filename_end - filename, stdout);
		printf("', '");
		fwrite(pattern, 1, pattern_end - pattern, stdout);
		printf("'\n");
	}
	if (pattern_end > pattern && memchr(pattern, '/', pattern_end - pattern)) {
		printf("'");
		fwrite(filename, 1, filename_end - filename, stdout);
		printf("', '");
		fwrite(pattern, 1, pattern_end - pattern, stdout);
		printf("'\n");
	}
	while (pattern < pattern_end - 1) {
		/* Split at "\-" pattern. */
		if (*pattern++ != '\\' || *pattern++ != '-')
			continue;
		result = cs_file_matches_pattern2(filename, filename_end,
						  pattern_start, pattern - 2);
		if (first)
			result = !result;
		if (result)
			return false;
		first = false;
		pattern_start = pattern;
	}
	result = cs_file_matches_pattern2(filename, filename_end,
					  pattern_start, pattern_end);
	return first ? result : !result;
}

/**
 * cs_path_matches_pattern2 - Do pathname pattern matching.
 *
 * @f: The start of string to check.
 * @p: The start of pattern to compare.
 *
 * Returns true if @f matches @p, false otherwise.
 */
static bool cs_path_matches_pattern2(const char *f, const char *p)
{
	const char *f_delimiter;
	const char *p_delimiter;
	while (*f && *p) {
		f_delimiter = strchr(f + 1, '/');
		if (!f_delimiter)
			f_delimiter = f + strlen(f);
		p_delimiter = strchr(p + 1, '/');
		if (!p_delimiter)
			p_delimiter = p + strlen(p);
		if (*p == '/' && *(p + 1) == '\\') {
			if (*(p + 2) == '(') {
				/* Check zero repetition. */
				if (cs_path_matches_pattern2(f, p_delimiter))
					return true;
				/* Check one or more repetition. */
				goto repetition;
			}
			if (*(p + 2) == '{')
				goto repetition;
		}
		if ((*f == '/' || *p == '/') && *f++ != *p++)
			return false;
		if (!cs_file_matches_pattern(f, f_delimiter, p, p_delimiter))
			return false;
		f = f_delimiter;
		p = p_delimiter;
	}
	/* Ignore trailing "\*" and "\@" in @pattern. */
	while (*p == '\\' && (*(p + 1) == '*' || *(p + 1) == '@'))
		p += 2;
	return !*f && !*p;
repetition:
	do {
		/* Compare current component with pattern. */
		if (!cs_file_matches_pattern(f + 1, f_delimiter,
					     p + 3, p_delimiter - 2))
			break;
		/* Proceed to next component. */
		f = f_delimiter;
		if (!*f)
			break;
		/* Continue comparison. */
		if (cs_path_matches_pattern2(f, p_delimiter))
			return true;
		f_delimiter = strchr(f + 1, '/');
	} while (f_delimiter);
	return false; /* Not matched. */
}

/**
 * cs_path_matches_pattern - Check whether the given filename matches the given pattern.
 *
 * @filename: The filename to check.
 * @pattern:  The pattern to compare.
 *
 * Returns true if matches, false otherwise.
 *
 * The following patterns are available.
 *   \ooo   Octal representation of a byte.
 *   \*     Zero or more repetitions of characters other than '/'.
 *   \@     Zero or more repetitions of characters other than '/' or '.'.
 *   \?     1 byte character other than '/'.
 *   \$     One or more repetitions of decimal digits.
 *   \+     1 decimal digit.
 *   \X     One or more repetitions of hexadecimal digits.
 *   \x     1 hexadecimal digit.
 *   \A     One or more repetitions of alphabet characters.
 *   \a     1 alphabet character.
 *
 *   \-     Subtraction operator.
 *
 *   /\{dir\}/   '/' + 'One or more repetitions of dir/' (e.g. /dir/ /dir/dir/
 *               /dir/dir/dir/ ).
 *
 *   /\(dir\)/   '/' + 'Zero or more repetitions of dir/' (e.g. / /dir/
 *               /dir/dir/ ).
 */
static bool cs_path_matches_pattern(const struct cs_path_info *filename,
				    const struct cs_path_info *pattern)
{
	const char *f = filename->name;
	const char *p = pattern->name;
	const int len = pattern->const_len;
	/* If @pattern doesn't contain pattern, I can use strcmp(). */
	if (len == pattern->total_len)
		return !cs_pathcmp(filename, pattern);
	/* Compare the initial length without patterns. */
	if (len) {
		if (strncmp(f, p, len))
			return false;
		f += len - 1;
		p += len - 1;
	}
	return cs_path_matches_pattern2(f, p);
}

/**
 * cs_correct_word - Check whether the given string follows the naming rules.
 *
 * @string:        The string to check.
 * @allow_pattern: True if allow use of patterns, false otherwise.
 *
 * Returns true if @string follows the naming rules, false otherwise.
 */
static bool cs_correct_word(const char *string, bool allow_pattern)
{
	u8 recursion = 20;
	const char *const start = string;
	u8 in_repetition = 0;
	if (!*string)
		goto out;
	while (*string) {
		unsigned char c = *string++;
		if (in_repetition && c == '/')
			goto out;
		if (c <= ' ' || c >= 127)
			goto out;
		if (c != '\\')
			continue;
		c = *string++;
		if (c >= '0' && c <= '3') {
			unsigned char d;
			unsigned char e;
			d = *string++;
			if (d < '0' || d > '7')
				goto out;
			e = *string++;
			if (e < '0' || e > '7')
				goto out;
			c = ((c - '0') << 6) + ((d - '0') << 3) + (e - '0');
			if (c <= ' ' || c >= 127 || c == '\\')
				continue;
			goto out;
		}
		if (!allow_pattern)
			goto out;
		switch (c) {
		case '+':   /* "\+" */
		case '?':   /* "\?" */
		case 'x':   /* "\x" */
		case 'a':   /* "\a" */
		case '-':   /* "\-" */
			continue;
		}
		/* Reject too deep wildcard that consumes too much stack. */
		if (!recursion--)
			goto out;
		switch (c) {
		case '*':   /* "\*" */
		case '@':   /* "\@" */
		case '$':   /* "\$" */
		case 'X':   /* "\X" */
		case 'A':   /* "\A" */
			continue;
		case '{':   /* "/\{" */
			if (string - 3 < start || *(string - 3) != '/')
				goto out;
			in_repetition = 1;
			continue;
		case '}':   /* "\}/" */
			if (in_repetition != 1 || *string++ != '/')
				goto out;
			in_repetition = 0;
			continue;
		case '(':   /* "/\(" */
			if (string - 3 < start || *(string - 3) != '/')
				goto out;
			in_repetition = 2;
			continue;
		case ')':   /* "\)/" */
			if (in_repetition != 2 || *string++ != '/')
				goto out;
			in_repetition = 0;
			continue;
		}
		goto out;
	}
	if (in_repetition)
		goto out;
	return true;
out:
	return false;
}

/**
 * cs_get_name - Allocate memory for string data.
 *
 * @name: The string to save.
 *
 * Returns pointer to "struct cs_path_info" on success, NULL otherwise.
 */
static struct cs_path_info *cs_get_name(const char *name)
{
	struct cs_path_info *ptr =
		(struct cs_path_info *) malloc(sizeof(struct cs_path_info));
	if (!ptr)
		out_of_memory();
	ptr->name = strdup(name);
	if (!ptr->name)
		out_of_memory();
	cs_fill_path_info(ptr);
	return ptr;
}

/**
 * cs_put_name - Free memory for string data.
 *
 * @name: Pointer to "struct cs_path_info". Maybe NULL.
 *
 * Returns nothing.
 */
static void cs_put_name(struct cs_path_info *name)
{
	if (name) {
		free((void *) name->name);
		free(name);
	}
}

/**
 * cs_normalize_line - Format string.
 *
 * @buffer: The line to normalize.
 *
 * Returns nothing.
 *
 * Leading and trailing whitespaces are removed.
 * Multiple whitespaces are packed into single space.
 */
static void cs_normalize_line(char *buffer)
{
	unsigned char *sp = (unsigned char *) buffer;
	unsigned char *dp = (unsigned char *) buffer;
	bool first = true;
	while (*sp && (*sp <= ' ' || *sp >= 127))
		sp++;
	while (*sp) {
		if (!first)
			*dp++ = ' ';
		first = false;
		while (*sp > ' ' && *sp < 127)
			*dp++ = *sp++;
		while (*sp && (*sp <= ' ' || *sp >= 127))
			sp++;
	}
	*dp = '\0';
}

static struct testcase {
	const char *pathname;
	const char *pattern;
	_Bool match;
} testcases[] = {
	{ "/tmp/000", "/tmp/\\*", 1 },
	{ "/tmp/000", "/tmp/\\@", 1 },
	{ "/tmp/000", "/tmp/\\?\\?\\?", 1 },
	{ "/tmp/000", "/tmp/\\*\\$\\@\\$", 1 },
	{ "/tmp/000\\040111", "/tmp/\\*", 1 },
	{ "/tmp/000\\040111", "/tmp/\\X", 0 },
	{ "/tmp/000\\040111", "/tmp/\\$\\?\\$", 1 },
	{ "/tmp/000111", "/tmp/\\(\\*\\)/\\*", 1 },
	{ "/tmp/000/111", "/tmp/\\(\\*\\)/\\*", 1 },
	{ "/tmp/0/0/0/1/1/1", "/tmp/\\(\\*\\)/\\*", 1 },
	{ "/tmp/0/0/0/1/1/1", "/tmp/\\{\\$\\}/\\$", 1 },
	{ "/tmp/0/0/0/1/1/1", "/tmp/\\{\\a\\}/\\$", 0 },
	{ "/tmp/0/0/0/1/1/1", "/tmp/\\{\\$\\-\\a\\}/\\$", 1 },
	{ "/tmp/0/0/0/1/1/1", "/tmp/\\{\\x\\-\\a\\}/\\$", 1 },
	{ "/tmp/\\001/\\002/\\040/^/$", "/tmp/\\{\\*\\-\\a\\}/\\?", 1 },
	{ "/tmp/\\001/\\002/\\040/^/$", "/tmp/\\{\\*\\-\\a\\-\\x\\}/\\?", 1 },
	{ "/tmp/$", "/tmp/\\*\\-\\a\\-\\x", 1 },
	{ "/bin/true", "/bin/\\*", 1 },
	{ "/bin/true", "/bin\\@\\*/\\*", 1 },
	{ "/usr/local/", "/usr/\\*/", 1 },
	{ "/usr/local/", "/usr/\\*\\*\\@\\*/", 1 },
	{ "pipe:[12345]", "pipe:[\\$]", 1 },
	{ "socket:[12345]", "socket:[\\$]", 1 },
	{ "https://tomoyo.osdn.jp/", "\\*/\\*/\\*/", 1 },
	{ "https://tomoyo.osdn.jp/index.html", "\\*/\\*/\\*/\\*", 1 },
	{ "https://tomoyo.osdn.jp/index.html", "\\*/\\*/\\*/\\*\\*\\@\\*\\@", 1 },
	{ "https://tomoyo.osdn.jp/index.html", "\\*/\\@\\*/\\*\\@/\\*\\@\\*\\@\\*", 1 },
	{ "https://tomoyo.osdn.jp/1.8/index.html", "https://\\{\\*\\}/\\@.html", 1 },
	{ "https://tomoyo.osdn.jp/index.html", "\\*://\\@.osdn.jp/\\*", 1 },
	{ "https://tomoyo.osdn.jp/index.html", "\\*://\\@.osdn.jp/\\*", 1 },
	{ "https://osdn.jp/projects/tomoyo/svn/view/trunk/1.8.x/ccs-patch/security/ccsecurity/?root=tomoyo", "\\*://\\@osdn.jp/\\{\\*\\}/?root=tomoyo", 1 },
	{ "https://osdn.jp/projects/tomoyo/svn/view/trunk/1.8.x/ccs-patch/security/?root=tomoyo", "\\*://\\@osdn.jp/\\{\\*\\}/?root=tomoyo", 1 },
	{ "https://osdn.jp/projects/tomoyo/svn/view/trunk/1.8.x/ccs-patch/?root=tomoyo", "\\*://\\@osdn.jp/\\{\\*\\}/?root=tomoyo", 1 },
	{ "https://osdn.jp/projects/tomoyo/svn/view/trunk/1.8.x//ccs-patch///security//ccsecurity///?root=tomoyo", "\\*://\\@osdn.jp/\\{\\*\\-.\\-..\\-\\*%\\*\\}/?root=tomoyo\\*\\*", 1 },
	{ "/var/www/html/test/test/test/index.html", "/var/www/html/\\{test\\}/\\*.html", 1 },
	{ "/etc/skel/", "/etc/\\{\\*\\}/\\*/", 0 },
	{ "/etc/passwd", "/etc/\\{\\*\\}/\\*", 0 },
	{ "/bin/true", "/bin/\\*/", 0 },
	{ "/bin/", "/bin/\\*", 1 },
	{ "/bin/", "/bin/\\@", 1 },
	{ "/bin/", "/bin/\\@\\@", 1 },
	{ "https://tomoyo.osdn.jp/", "\\*/\\*/\\*/\\?", 0 },
	{ "https://tomoyo.osdn.jp/index.html", "\\*/\\*/\\*/\\@", 0 },
	{ "https://tomoyo.osdn.jp/index.html", "https://\\*/\\@", 0 },
	{ "socket:[12345]", "/\\{\\*\\}/socket:[\\*]", 0 },
	{ "/", "/\\(\\*\\)/\\*", 1 },
	{ "/", "/\\{\\*\\}/\\*", 0 },
	{ "/foo/", "/foo/\\(\\*\\)/\\(\\*\\)/\\(\\*\\)/\\(\\*\\)/\\(\\*\\)/\\*", 1 },
	{ "/foo/", "/foo/\\{\\*\\}/\\*", 0 },
	{ "/foo/bar/", "/foo/\\(\\*\\)/\\(\\*\\)/\\(\\*\\)/\\(\\*\\)/\\(\\*\\)/\\*", 1 },
	{ "/foo/bar/", "/foo/\\{\\*\\}/\\*", 1 },
	{ "/foo/bar", "/foo/\\(\\*\\)/\\(\\*\\)/\\(\\*\\)/\\(\\*\\)/\\(\\*\\)/bar", 1 },
	{ "/foo/bar", "/foo/\\{\\*\\}/bar", 0 },
	{ "/foo/bar", "/foo/\\*/\\(\\*\\)/\\(\\*\\)/\\(\\*\\)/\\(\\*\\)/\\(\\*\\)/\\*", 0 },
	{ "/foo/bar/", "/foo/\\(\\*\\)/", 1 },
	{ "/foo/bar/", "/foo/\\*/\\(\\*\\)/", 1 },
	{ "/foo/bar/", "/foo/\\*/\\*/\\(\\*\\)/", 0 },
	{ "/foo/bar/", "/foo/\\*/\\(\\*\\)/\\*/", 0 },
	{ "/foo/", "/foo/\\(\\*\\)/", 1 },
	{ "abc", "abc", 1 },
	{ "abc", "\\A\\A\\A", 1 },
	{ "abc", "\\X\\X\\X", 1 },
	{ "abc", "\\*\\*\\*", 1 },
	{ "abc", "\\@\\@\\@", 1 },
	{ "abc", "\\a\\@\\x", 1 },
	{ "abc", "\\?\\?\\?", 1 },
	{ "abc", "\\?\\@\\?\\*", 1 },
	{ "abc", "\\?\\@\\?\\*\\?", 1 },
	{ "abc", "def", 0 },
	{ "abc/def", "\\*/\\(\\X\\)/\\X", 1 },
	{ "abc/def", "\\*/\\{\\X\\}/\\X", 0 },
	{ "abc/def/012", "\\*/\\(\\X\\)/\\X", 1 },
	{ "abc/def/012", "\\*/\\{\\X\\}/\\X", 1 },
	{ "abc/def/012/345/6789", "\\*/\\(\\X\\)/\\X", 1 },
	{ "abc/def/012/345/6789", "\\*/\\{\\X\\}/\\X", 1 },
	{ "abc/345/012/def/6789", "\\*/\\(\\$\\)/\\X", 0 },
	{ "abc/345/012/def/6789", "\\*/\\(\\*\\)/\\*/", 0 },
	{ "abc/345/012/def/6789/////1//23///", "\\*/\\(\\*\\)/\\*/", 1 },
	{ "abc/345/012/def/6789/////1//23//./", "\\*/\\(\\*\\)/\\?/", 1 },
	{ "abc/345/012/def/6789//1//23//.", "\\*/\\(\\*\\)/\\?/", 0 },
	{ "abc/345/012/def/6789//1//23//", "\\*/\\(\\*\\)/\\?", 0 },
	{ "abc", "abc/\\*", 0 },
	{ "abc/", "abc/\\*", 1 },
	{ NULL, NULL, 0 },
};

int main(int argc, char *argv[])
{
	struct testcase *ptr;
	struct cs_path_info *path;
	struct cs_path_info *pattern;
	for (ptr = testcases; ptr->pathname; ptr++) {
		if (!cs_correct_word(ptr->pathname, 0)) {
			printf("Bad path: %s\n", ptr->pathname);
			continue;
		} else if (!cs_correct_word(ptr->pattern, 1)) {
			printf("Bad pattern: %s\n", ptr->pattern);
			continue;
		}
		path = cs_get_name(ptr->pathname);
		pattern = cs_get_name(ptr->pattern);
		if (cs_path_matches_pattern(path, pattern) != ptr->match)
			printf("Failed (\"%s\", \"%s\") == %d\n",
			       ptr->pathname, ptr->pattern, ptr->match);
		cs_put_name(path);
		cs_put_name(pattern);
	}
	return 0;
}
