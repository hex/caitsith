/*
 * caitsith-init.c
 *
 * CaitSith's utilities.
 *
 * Copyright (C) 2005-2012  NTT DATA CORPORATION
 *
 * Version: 0.2   2016/10/05
 *
 * This program is executed automatically by kernel
 * when execution of /sbin/init is requested.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License v2 as published by the
 * Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 */
#define _FILE_OFFSET_BITS 64
#define _LARGEFILE_SOURCE
#define _LARGEFILE64_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/mount.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <limits.h>
#include <sys/vfs.h>
#include <errno.h>

static void panic(void)
{
	printf("Fatal error while loading policy.\n");
	fflush(stdout);
	while (1)
		sleep(100);
}

#define policy_dir  "/etc/caitsith/policy"
static const char *proc_policy = "/sys/kernel/security/caitsith/policy";
static _Bool proc_unmount = 0;
static _Bool sys_unmount = 0;
static _Bool security_unmount = 0;

static char buffer[8192];

static void copy_files(const char *src, const char *dest)
{
	int sfd;
	int dfd = open(dest, O_WRONLY);
	if (dfd == EOF) {
		if (errno != ENOENT)
			panic();
		return;
	}
	sfd = open(src, O_RDONLY);
	if (sfd != EOF) {
		while (1) {
			int ret_ignored;
			int len = read(sfd, buffer, sizeof(buffer));
			if (len <= 0)
				break;
			ret_ignored = write(dfd, buffer, len);
		}
		close(sfd);
	}
	close(dfd);
}

static void show_stat(void)
{
	unsigned int acl = 0;
	unsigned int size = -1;
	FILE *fp = fopen(proc_policy, "r");
	if (!fp)
		return;
	while (memset(buffer, 0, sizeof(buffer)) &&
	       fgets(buffer, sizeof(buffer) - 1, fp)) {
		unsigned int priority;
		unsigned char operation;
		if (sscanf(buffer, "%u acl %c", &priority, &operation) == 2)
			acl++;
		else if (size == -1)
			sscanf(buffer, "stat Memory used by policy: %u",
			       &size);
	}
	fclose(fp);
	printf("%u ACL entr%s.\n", acl, acl > 1 ? "ies" : "y");
	if (size != -1)
		printf("%u KB used by policy.\n", (size + 1023) / 1024);
}

int main(int argc, char *argv[])
{
	struct stat buf;

	/* Mount /proc if not mounted. */
	if (lstat("/proc/self/", &buf) || !S_ISDIR(buf.st_mode))
		proc_unmount = !mount("/proc", "/proc/", "proc", 0, NULL);
	/* Mount /sys if not mounted. */
	if (lstat("/sys/kernel/security/", &buf) || !S_ISDIR(buf.st_mode))
		sys_unmount = !mount("/sys", "/sys", "sysfs", 0, NULL);
	/* Mount /sys/kernel/security if not mounted. */
	if (lstat("/sys/kernel/security/caitsith/", &buf) ||
	    !S_ISDIR(buf.st_mode))
		security_unmount = !mount("none", "/sys/kernel/security",
					  "securityfs", 0, NULL);

	/*
	 * Open /dev/console if stdio are not connected.
	 *
	 * WARNING: Don't let this program be invoked implicitly
	 * if you are not operating from console.
	 * Otherwise, you will get unable to respond to prompt
	 * if something went wrong.
	 */
	if (access("/proc/self/fd/0", R_OK)) {
		close(0);
		close(1);
		close(2);
		open("/dev/console", O_RDONLY);
		open("/dev/console", O_WRONLY);
		open("/dev/console", O_WRONLY);
	}

	/* Load kernel module if needed. */
	if (lstat(proc_policy, &buf) && lstat("/proc/caitsith", &buf)) {
		if (!access("/etc/caitsith/caitsith-load-module", X_OK)) {
			const pid_t pid = fork();
			switch (pid) {
			case 0:
				execl("/etc/caitsith/caitsith-load-module",
				      "/etc/caitsith/caitsith-load-module",
				      NULL);
				_exit(0);
			case -1:
				panic();
			}
			while (waitpid(pid, NULL, __WALL) == EOF &&
			       errno == EINTR);
		}
	}

	/* Try proc interface if securityfs interface does not exist. */
	if (lstat(proc_policy, &buf) || !S_ISREG(buf.st_mode))
		proc_policy = "/proc/caitsith/policy";

	/* Stop if policy interface doesn't exist. */
	if (lstat(proc_policy, &buf) || !S_ISREG(buf.st_mode)) {
		printf("FATAL: Policy interface %s does not exist.\n",
		       proc_policy);
		fflush(stdout);
		while (1)
			sleep(100);
	}

	/*
	 * Unmount and execute /sbin/init if this program was executed by
	 * passing init=/sbin/caitsith-init . The kernel will try to execute
	 * this program again with getpid() != 1 when /sbin/init starts.
	 */
	if (getpid() == 1) {
		if (security_unmount)
			umount("/sys/kernel/security/");
		if (sys_unmount)
			umount("/sys/");
		if (proc_unmount)
			umount("/proc/");
		argv[0] = "/sbin/init";
		execv(argv[0], argv);
		printf("FATAL: Failed to execute %s\n", argv[0]);
		fflush(stdout);
		while (1)
			sleep(100);
	}

	/* Load policy. */
	if (!chdir(policy_dir))
		copy_files("current", proc_policy);

	/* Do additional initialization. */
	if (!access("/etc/caitsith/caitsith-post-init", X_OK)) {
		const pid_t pid = fork();
		switch (pid) {
		case 0:
			execl("/etc/caitsith/caitsith-post-init",
			      "/etc/caitsith/caitsith-post-init", NULL);
			_exit(0);
		case -1:
			panic();
		}
		while (waitpid(pid, NULL, __WALL) == EOF &&
		       errno == EINTR);
	}

	show_stat();

	if (security_unmount)
		umount("/sys/kernel/security/");
	if (sys_unmount)
		umount("/sys/");
	if (proc_unmount)
		umount("/proc");

	return 0;
}
